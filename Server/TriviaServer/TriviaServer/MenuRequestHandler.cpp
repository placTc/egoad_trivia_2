#include "MenuRequestHandler.h"
#include "RequestResult.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "SqliteDatabase.h"
#include "CreateRoomResponse.h"
#include "JoinRoomResponse.h"
#include "GetPlayersInRoomResponse.h"
#include "GetRoomsResponse.h"
#include "LogoutResponse.h"
#include <mutex>
#include <thread>

MenuRequestHandler::MenuRequestHandler(std::string user):
    m_user(LoggedUser(user))
{
}

MenuRequestHandler::~MenuRequestHandler()
{
}

bool MenuRequestHandler::isReqeustRelevant(RequestInfo ri)
{
    std::string s = "EJCPOR";
    if (s.find(ri.id) != std::string::npos)
    {
        return true;
    }
    return false;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo ri)
{
    std::unique_lock<std::mutex> roomLock(this->roomsMtx, std::defer_lock);
    SqliteDatabase sqlDatabase;
    sqlDatabase.open();
    LoginManager loginManager(&sqlDatabase);
    RoomManager rm;

    RequestResult rr;
    rr.newHandler = RequestHandlerFactory::createMenuRequestHandler(m_user.getUsername());
    std::vector<unsigned char> buf;

    try
    {
        roomLock.lock();
        if (ri.id == CREATEROOM_CODE)
        {
            CreateRoomRequest roomRequest = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(ri.buffer, ri.buffer.size());
            int roomID = (rm.getRooms().empty()) ? 1 : rm.getRooms()[rm.getRooms().size() - 1].id + 1;
            RoomData roomData = RoomData(roomID, roomRequest.roomName, roomRequest.maxUsers, roomRequest.questionCount, roomRequest.answerTimeout, false);
            rm.createRoom(m_user, roomData);

            CreateRoomResponse resp;
            resp.status = roomID;
            std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(resp);
            rr.buffer = buf;

            rr.newHandler = RequestHandlerFactory::createMenuRequestHandler(m_user.getUsername());
        }
        else if (ri.id == JOINROOM_CODE)
        {
            JoinRoomRequest joinRequest = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(ri.buffer, ri.buffer.size());
            if (rm.m_rooms[joinRequest.roomId].getData().maxPlayers != rm.m_rooms[joinRequest.roomId].getAllUsers().size())
            {
                rm.m_rooms[joinRequest.roomId].addUser(m_user);

                JoinRoomResponse resp;
                resp.status = 1;
                std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(resp);
                rr.buffer = buf;

                rr.newHandler = RequestHandlerFactory::createMenuRequestHandler(m_user.getUsername());
            }
            else
            {
                ErrorResponse err;
                err.message = "Too many users in room";
                std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(err);
                rr.buffer = buf;

                rr.newHandler = RequestHandlerFactory::createMenuRequestHandler(m_user.getUsername());
            }
        }
        else if (ri.id == GETPLAYERS_CODE)
        {
            GetPlayersInRoomRequest getPlayersRequest = JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(ri.buffer, ri.buffer.size());
            std::vector<std::string> users = rm.m_rooms[getPlayersRequest.roomId].getAllUsers();

            GetPlayersInRoomResponse resp;
            resp.players = users;
            resp.status = 1;
            std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(resp);

            rr.buffer = buf;
            rr.newHandler = RequestHandlerFactory::createMenuRequestHandler(m_user.getUsername());
        }
        else if (ri.id == GETROOMS_CODE)
        {
            GetRoomsResponse getRooms;
            getRooms.rooms = rm.getRooms();
            getRooms.status = 1;
            std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(getRooms);

            rr.buffer = buf;
            rr.newHandler = RequestHandlerFactory::createMenuRequestHandler(m_user.getUsername());
        }
        else if (ri.id == LOGOUT_CODE)
        {
            loginManager.logout(this->m_user.getUsername());

            std::map<int, Room>::iterator it;
            bool userDetected = false;

            for (it = rm.m_rooms.begin(); it != rm.m_rooms.end(); it++)
            {
                Room r = it->second;
                std::vector<std::string> users = r.getAllUsers();
                for (int i = 0; i < users.size(); i++)
                {
                    if (users[i] == m_user.getUsername())
                    {
                        it->second.removeUser(m_user);
                        userDetected = true;
                        break;
                    }
                }
                if (it->second.getAllUsers().empty())
                {
                    rm.deleteRoom(it->second.getData().id);
                }
                if (userDetected)
                    break;
            }


            LogoutResponse logoutResp;
            logoutResp.status = 1;

            std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(logoutResp);
            rr.buffer = buf;
            rr.newHandler = RequestHandlerFactory::createLoginRequestHandler();
        }
        roomLock.unlock();
    }
    catch (...)
    {
        if (roomLock.owns_lock())
        {
            roomLock.unlock();
        }
    }
    return rr;
}
