#pragma once
#include "RoomData.h"
#include "LoggedUser.h"
#include <vector>

class Room
{
private:
	RoomData m_metadata;
	std::vector<LoggedUser> m_users;

public:
	Room();
	Room(RoomData data);
	~Room();
	void addUser(LoggedUser user);
	void removeUser(LoggedUser user);
	std::vector<std::string> getAllUsers();
	bool getState();
	RoomData getData();
};

