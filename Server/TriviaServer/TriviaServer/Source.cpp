#include "Server.h"
#include "Helper.h"
#include "WSAInitializer.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include <iostream>

std::vector<LoggedUser> LoginManager::m_loggedUsers = std::vector<LoggedUser>();
std::map<int, Room> RoomManager::m_rooms = std::map<int, Room>();

int main(void)
{
	WSAInitializer wsai;
	Server server;
	server.run();
	WSACleanup();
}
