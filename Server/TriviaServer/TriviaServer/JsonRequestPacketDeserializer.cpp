#include "JsonRequestPacketDeserializer.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer, int length)
{
    std::string s(reinterpret_cast<char*>(buffer.data()));
    s.resize(length);
    json j = json::parse(s);

    return LoginRequest(j["username"], j["password"]);
}

SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer, int length)
{
    std::string s(reinterpret_cast<char*>(buffer.data()));
    s.resize(length);
    json j = json::parse(s);

    return SignupRequest(j["username"], j["password"], j["email"], j["address"], j["phoneNumber"], j["birthDate"]);
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer, int length)
{
    std::string s(reinterpret_cast<char*>(buffer.data()));
    s.resize(length);
    json j = json::parse(s);

    return GetPlayersInRoomRequest(j["roomID"]);
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer, int length)
{
    std::string s(reinterpret_cast<char*>(buffer.data()));
    s.resize(length);
    json j = json::parse(s);

    return JoinRoomRequest(j["roomID"]);
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer, int length)
{
    std::string s(reinterpret_cast<char*>(buffer.data()));
    s.resize(length);
    json j = json::parse(s);

    return CreateRoomRequest(j["roomName"], j["maxUsers"], j["questionCount"], j["answerTime"]);
}
