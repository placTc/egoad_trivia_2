#pragma once
#include <ctime>
#include <vector>
#include <string>

class RequestInfo
{
public:
	char id;
	std::time_t time;
	std::vector<unsigned char> buffer;

	RequestInfo(int id, std::string buffer);
	~RequestInfo();
};

