#include "CreateRoomRequest.h"

CreateRoomRequest::CreateRoomRequest(std::string roomName, int maxUsers, int questionCount, int answerTimeout)
{
	this->roomName = roomName;
	this->maxUsers = maxUsers;
	this->questionCount = questionCount;
	this->answerTimeout = answerTimeout;
}

CreateRoomRequest::~CreateRoomRequest()
{
}
