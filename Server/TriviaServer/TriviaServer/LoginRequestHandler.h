#pragma once
#include "IRequestHandler.h"
#include "RequestInfo.h"
#include "RequestResult.h"
#include "RequestHandlerFactory.h"

#define LOGIN_CODE 'L'
#define SGNUP_CODE 'S'
#define ERROR_CODE 'E'

class LoginRequestHandler :
    public IRequestHandler
{
public:
    LoginRequestHandler();
    ~LoginRequestHandler();
    bool isReqeustRelevant(RequestInfo ri);
    RequestResult handleRequest(RequestInfo ri);
};

