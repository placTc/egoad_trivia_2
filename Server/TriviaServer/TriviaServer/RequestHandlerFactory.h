#pragma once

#include "RoomManager.h"
#include "LoginManager.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
class RequestHandlerFactory
{
private:
	//LoginManager m_loginManager;
	//RoomManager m_roomManager;
	//StatisticsManager m_statisticsManager;
	//IDatabase* m_database;
public:
	static IRequestHandler* createLoginRequestHandler();
	static IRequestHandler* createMenuRequestHandler(std::string user);

};

