#include "SqliteDatabase.h"
#include <cstdlib> 
#include <ctime> 

bool SqliteDatabase::open()
{
	std::string dbFileName = DB_NAME;
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &(this->_db));
	if (res != 0) {
		_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}
	if (doesFileExist == -1)
	{
		char* errMessage = nullptr;

		string sqlStatement = "CREATE TABLE Game ( \
			game_id INTEGER, \
			PRIMARY KEY(game_id) \
			)";
		res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), nullptr, nullptr, &errMessage);
		sqlStatement = "CREATE TABLE Question (\
			question	TEXT,\
			ans1	TEXT,\
			ans2	TEXT,\
			ans3	TEXT,\
			ans4	TEXT,\
			correct_ans_id	INTEGER)";

		insertQuestions(QuestionsFile, _db);

		res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), nullptr, nullptr, &errMessage);
		sqlStatement = "CREATE TABLE User ( \
			username	TEXT, \
			password	TEXT, \
			email	TEXT, \
			address TEXT, \
			phoneNumber TEXT, \
			birthDate TEXT, \
			PRIMARY KEY(username) \
			)";
		res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), nullptr, nullptr, &errMessage);

	}

	return true;
}

void SqliteDatabase::close()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}

void SqliteDatabase::clear()
{
	char* errMessage = nullptr;

	string sqlStatement = "delete from game;";
	int res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), nullptr, nullptr, &errMessage);
	sqlStatement = "delete from question;";
	res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), nullptr, nullptr, &errMessage);
	sqlStatement = "delete from user;";
	res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), nullptr, nullptr, &errMessage);
}

bool SqliteDatabase::doesUserExist(string username)
{
	char* errMessage = nullptr;
	string username_from_db;
	string sqlStatement = "SELECT username FROM User WHERE username LIKE \"" + username + "\";";

	int res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), get_user_callback, &username_from_db, &errMessage);

	if (res != SQLITE_OK)
	{
		std::cout << "Failed getting user" << std::endl;
	}
	
	return username_from_db.size()!=0;
}

bool SqliteDatabase::doesPasswordMatch(string username, string password)
{
	char* errMessage = nullptr;
	string password_from_db;
	string sqlStatement = "SELECT password FROM User WHERE username == \"" + username + "\";";

	int res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), get_password_callback, &password_from_db, &errMessage);

	if (res != SQLITE_OK)
	{
		std::cout << "Failed getting password" << std::endl;
	}

	return password_from_db == password;
}

void SqliteDatabase::addNewUser(string username, string password, string email, string address, string phoneNumber, string birthDate)
{
	char* errMessage = nullptr;
	string password_from_db;
	string sqlStatement = "INSERT INTO user (username, password, email, address, phoneNumber, birthDate) VALUES(\"" + username + "\", \"" + password + "\", \"" + email + "\", \"" + address + "\", \"" + phoneNumber + "\", \"" + birthDate + "\");";

	int res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), nullptr, nullptr, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed adding user" << errMessage << std::endl;
	}
}

std::list<Question> SqliteDatabase::getQuestions(int num) // gets number of random questions
{
	std::srand(std::time(nullptr));
	std::vector<Question> q_vec;
	std::list<Question> ret_list;
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM question" + std::to_string(num) + ";";

	int res = sqlite3_exec(_db, (sqlStatement + ";").c_str(), get_questions_callback, &q_vec, &errMessage);
	if (res != SQLITE_OK)
	{
		std::cout << "Failed getting questions" << errMessage << std::endl;
	}

	if (num < q_vec.size())
	{
		for (int i = 0; i < num; i++)
		{
			int x = std::rand()% q_vec.size();
			ret_list.push_back(q_vec[x]);
			q_vec.erase(q_vec.begin() + x);
		}
	}
	

	return ret_list;
}

int SqliteDatabase::getSecurityKey(string)
{
	return 0;
}

std::vector<string> SqliteDatabase::split(string str, string token)
{
	std::vector<string>result;
	while (str.size()) {
		int index = str.find(token);
		if (index != string::npos) {
			result.push_back(str.substr(0, index));
			str = str.substr(index + token.size());
			if (str.size() == 0)result.push_back(str);
		}
		else {
			result.push_back(str);
			str = "";
		}
	}
	return result;
}

void SqliteDatabase::insertQuestions(string fileName, sqlite3* db)
{
	std::vector<string> questionsAndAnswers;
	std::vector<Question> questions;

	std::ifstream file = std::ifstream("QuestionsToLoad.txt");
	std::stringstream buffer;
	buffer << file.rdbuf();
	std::string file_content = buffer.str();
	questionsAndAnswers = split(file_content, "@@");
	for (int i = 0; i < questionsAndAnswers.size(); i++)
	{
		if (questionsAndAnswers[i].find('\n') != -1)
		{
			std::vector<string> tempQuestionAndAnswer;
			tempQuestionAndAnswer = split(questionsAndAnswers[i], "\n");
			std::vector<string> answers;
			Question temp;
			temp.set_question(tempQuestionAndAnswer[0]);
			for (int j = 1; j < tempQuestionAndAnswer.size() - 1; j++)
			{
				answers.push_back(tempQuestionAndAnswer[j]);
			}
			for (int k = 0; k < answers.size(); k++)
			{
				if (answers[k][answers[k].size() - 1] == '*')
				{
					temp.set_correct_ans_id(k + 1);
					answers[k].resize(answers[k].size() - 1);
				}
			}
			temp.set_possibleAnswers(answers);
			questions.push_back(temp);
		}
	}

	for (int i = 0; i < questions.size(); i++)
	{
		char* errMessage = nullptr;
		string password_from_db;
		string sqlStatement = "INSERT INTO Question VALUES(\"" + questions[i].getQuestion();

		for (int j = 0; j < 4; j++)
		{
			if (j < questions[i].getPossibleAnswers().size())
			{
				sqlStatement += "\", \"" + questions[i].getPossibleAnswers()[j];
			}
			else
			{
				sqlStatement += "\", \"empty";
			}
		}
		sqlStatement += "\", \"" + std::to_string(questions[i].getCorrectAnsId()) + "\");";
		std::cout << sqlStatement << std::endl;
		int res = sqlite3_exec(db, (sqlStatement + ";").c_str(), nullptr, nullptr, &errMessage);
		if (res != SQLITE_OK)
		{
			std::cout << "Failed adding user" << errMessage << std::endl;
		}
	}
}

int SqliteDatabase::get_user_callback(void* data, int argc, char** argv, char** azColName)
{
	string* username = (string*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "username") {
			string s(argv[i]);
			*username = s;
		}
	}
	return 0;
}

int SqliteDatabase::get_password_callback(void* data, int argc, char** argv, char** azColName)
{
	string* password = (string*)data;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "password") {
			string s(argv[i]);
			*password = s;
		}
	}
	return 0;
}

int SqliteDatabase::get_questions_callback(void* data, int argc, char** argv, char** azColName)
{
	std::vector<Question>* question_list = (std::vector<Question>*)data;
	Question q1;

	for (int i = 0; i < argc; i++) {
		std::vector<string> answers;
		if (std::string(azColName[i]) == "question") {
			q1.set_question(azColName[i]);
		}
		else if (std::string(azColName[i]) == "ans1") {
			answers.push_back(azColName[i]);
		}
		else if (std::string(azColName[i]) == "ans2") {
			answers.push_back(azColName[i]);
		}
		else if (std::string(azColName[i]) == "ans3") {
			answers.push_back(azColName[i]);
		}
		else if (std::string(azColName[i]) == "ans4") {
			answers.push_back(azColName[i]);
		}
		else if (std::string(azColName[i]) == "correct_ans_id") {
			q1.set_correct_ans_id(std::stoi(azColName[i]));
		}
		q1.set_possibleAnswers(answers);
	}
	return 0;
}


