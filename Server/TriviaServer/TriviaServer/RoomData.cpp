#include "RoomData.h"

RoomData::RoomData() :
	id(0), name(""), maxPlayers(0), numOfQuestionsInGame(0), timePerQuestion(0), isActive(false)
{
}

RoomData::RoomData(unsigned int id, std::string name, unsigned int maxPlayers, unsigned int numOfQuestions, unsigned int timePerQuestion, bool isActive):
	id(id), name(name), maxPlayers(maxPlayers), numOfQuestionsInGame(numOfQuestions), timePerQuestion(timePerQuestion), isActive(isActive)
{
}

RoomData::~RoomData()
{
}
