#include "Communicator.h"
#include <thread>
#include <mutex>
#include <iostream>

void Communicator::bindAndListen()
{
	std::unique_lock<std::mutex> socketLock(this->socketMtx, std::defer_lock); 
	std::unique_lock<std::mutex> mapLock(this->mapMtx, std::defer_lock);

	socketLock.lock();
	this->m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP); // define initial socket

	DWORD timeouttime = INFINITE;
	BOOL reuseaddr = true;
	auto iResult = ::setsockopt(this->m_serverSocket, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuseaddr, sizeof(reuseaddr)); // set socket option to reset port
	if (iResult == SOCKET_ERROR) {
		std::cout << 9 << std::endl;
		printf("setsockopt failed with error: %d\n", WSAGetLastError());
	}
	auto iResult2 = ::setsockopt(this->m_serverSocket, SOL_SOCKET, SO_RCVTIMEO, (const char*)&timeouttime, sizeof(timeouttime)); // set socket option to reset port
	if (iResult2 == SOCKET_ERROR) {
		std::cout << 9 << std::endl;
		printf("setsockopt failed with error: %d\n", WSAGetLastError());
	}


	if (this->m_serverSocket == INVALID_SOCKET)
	{
		wprintf(L"socket function failed with error = %d\n", WSAGetLastError());
		throw std::exception(__FUNCTION__ " - socket");
	}

	struct sockaddr_in sock;

	sock.sin_port = htons(PORT);
	sock.sin_family = AF_INET;
	sock.sin_addr.s_addr = INADDR_ANY;

	if (bind(this->m_serverSocket, (struct sockaddr*)&sock, sizeof(sock)) == SOCKET_ERROR)
	{
		wprintf(L"socket function failed with error = %d\n", WSAGetLastError());
		throw std::exception(__FUNCTION__ " - bind");
	}

	if (listen(this->m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
	{
		wprintf(L"socket function failed with error = %d\n", WSAGetLastError());
		throw std::exception(__FUNCTION__ " - listen");
	}

	SOCKET client_socket = accept(this->m_serverSocket, NULL, NULL); // start listening for a connection
	closesocket(this->m_serverSocket); // close intial socket

	if (client_socket == INVALID_SOCKET)
	{
		wprintf(L"socket function failed with error = %d\n", WSAGetLastError());
		throw std::exception(__FUNCTION__);
	}
	socketLock.unlock(); 

	mapLock.lock();
	IRequestHandler* requestHandler = new LoginRequestHandler();
	m_clients.emplace(client_socket, requestHandler); // save new socket in map
	mapLock.unlock();

	handleNewClient(client_socket, requestHandler); // move on to handling the client
}

void Communicator::handleNewClient(SOCKET sock, IRequestHandler* handler)
{
	std::unique_lock<std::mutex> iostreamLock(this->iostreamMtx, std::defer_lock);
	char code;
	std::string data;
	std::vector<unsigned char> lengthInBytes;
	int length;
	while (true)
	{
		try
		{

			code = Helper::getCharPartFromSocket(sock); // getting message code
			if (!Helper::isCodeCorrect(code))
			{
				continue;
			}
			for (int i = 0; i < 4; i++)
				lengthInBytes.push_back(Helper::getCharPartFromSocket(sock)); // getting message length, byte by byte

			std::reverse(lengthInBytes.begin(), lengthInBytes.end());
			std::memcpy(&length, reinterpret_cast<const unsigned char*>(lengthInBytes.data()), sizeof(int)); // creating usable integer out of bytes


			data = Helper::getStringPartFromSocket(sock, length); // get data
			data.resize(length);

			RequestInfo requestInfo = RequestInfo(code, data);
			RequestResult requestResult;

			if (handler->isReqeustRelevant(requestInfo))
			{
				requestResult = handler->handleRequest(requestInfo); // handle given request to prepare for deserialization
				delete handler;
				handler = requestResult.newHandler;
				send(sock, reinterpret_cast<const char*>(requestResult.buffer.data()), requestResult.buffer.size(), 0);
			}
			else
			{
				continue;
			}
                                                                                                                                                                   		}
		catch (...) // emergency disconnect
		{
			if (iostreamLock.owns_lock())
			{
				iostreamLock.unlock();
			}
			break;
		}
	}
}

Communicator::Communicator()
{
	this->m_serverSocket = INVALID_SOCKET;
}

Communicator::~Communicator() noexcept(false)
{
	for (std::map<SOCKET, IRequestHandler*>::iterator it = m_clients.begin(); it != m_clients.end(); ++it)
	{
		try
		{
			closesocket(it->first);
		}
		catch (int i) {}
	}
}

void Communicator::startHandleRequests()
{
	std::unique_lock<std::mutex> mapLock(this->mapMtx, std::defer_lock);
	m_clients.emplace(0, new LoginRequestHandler());

	int currentLength = 1;
	std::vector<std::thread> threads;
	bool run = true;

	threads.emplace_back(&Communicator::bindAndListen, this);
	threads.back().detach();

	while (run)
	{
		if (currentLength < m_clients.size())
		{
			mapLock.lock();
			currentLength = m_clients.size();
			mapLock.unlock();

			threads.emplace_back(&Communicator::bindAndListen, this);
			threads.back().detach();
		}
	}

	for (int i = 0; i < threads.size(); i++)
	{
		if (threads[i].joinable())
		{
			threads[i].detach();
		}
	}
}
