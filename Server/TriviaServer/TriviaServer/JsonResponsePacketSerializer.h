#pragma once
#include "json.hpp"
#include "ErrorResponse.h"
#include "LoginResponse.h"
#include "SignupResponse.h"
#include "CreateRoomResponse.h"
#include "JoinRoomResponse.h"
#include "GetPlayersInRoomResponse.h"
#include "GetRoomsResponse.h"
#include "LogoutResponse.h"
#include <vector>

using json = nlohmann::json;

#define ERROR_CODE 'E'
#define LOGIN_CODE 'L'
#define SGNUP_CODE 'S'

#define JOINROOM_CODE 'J'
#define CREATEROOM_CODE 'C'
#define GETPLAYERS_CODE 'P'
#define LOGOUT_CODE 'O'
#define GETROOMS_CODE 'R'

class JsonResponsePacketSerializer
{
public:
	static std::vector<unsigned char> serializeResponse(ErrorResponse resp);
	static std::vector<unsigned char> serializeResponse(LoginResponse resp);
	static std::vector<unsigned char> serializeResponse(SignupResponse resp);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse resp);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse resp);
	static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse resp);
	static std::vector<unsigned char> serializeResponse(GetRoomsResponse resp);
	static std::vector<unsigned char> serializeResponse(LogoutResponse resp);
private:
	static std::vector<unsigned char> intToBytes(int paramInt);
};

