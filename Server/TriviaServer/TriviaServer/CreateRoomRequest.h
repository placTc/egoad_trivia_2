#pragma once
#include <string>
class CreateRoomRequest
{
public:
	CreateRoomRequest(std::string roomName, int maxUsers, int questionCount, int answerTimeout);
	~CreateRoomRequest();
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

