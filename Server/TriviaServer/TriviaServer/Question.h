#pragma once

#include <string>
#include <vector>

using std::string;

class Question
{
private:
	string m_question;
	std::vector<string> m_possibleAnswers;
	int m_correct_ans_id;
public:
	Question(string question, std::vector<string> possibleAnswers, int correct_ans_id);
	Question();
	~Question();
	void set_question(string question);
	void set_possibleAnswers(std::vector<string> possibleAnswers);
	void set_correct_ans_id(int id);
	string getQuestion();
	std::vector<string> getPossibleAnswers();
	string getCorrentAnswer();
	int getCorrectAnsId();
};

