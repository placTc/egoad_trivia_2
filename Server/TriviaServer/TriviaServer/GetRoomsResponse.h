#pragma once
#include "RoomData.h"
#include <vector>
class GetRoomsResponse
{
public:
	std::vector<RoomData> rooms;
	unsigned int status;
	GetRoomsResponse();
	~GetRoomsResponse();
};

