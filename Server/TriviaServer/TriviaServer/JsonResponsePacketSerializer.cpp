#include "JsonResponsePacketSerializer.h"


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse resp)
{
    json j;
    std::vector<unsigned char> preSerialized;
    std::vector<unsigned char> helper;

    preSerialized.push_back(ERROR_CODE);
    j["response"] = resp.message;

    helper = intToBytes(j.dump().length());
    preSerialized.insert(preSerialized.end(), helper.begin(), helper.end());

    std::string jsonString = j.dump();
    for (int i = 0; i < jsonString.length(); i++)
    {
        preSerialized.push_back(jsonString[i]);
    }

    return preSerialized;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse resp)
{
    json j;
    std::vector<unsigned char> preSerialized;
    std::vector<unsigned char> helper;

    preSerialized.push_back(LOGIN_CODE);
    j["response"] = resp.status;

    helper = intToBytes(j.dump().length());
    preSerialized.insert(preSerialized.end(), helper.begin(), helper.end());

    std::string jsonString = j.dump();
    for (int i = 0; i < jsonString.length(); i++)
    {
        preSerialized.push_back(jsonString[i]);
    }

    return preSerialized;
}
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse resp)
{
    json j;
    std::vector<unsigned char> preSerialized;
    std::vector<unsigned char> helper;

    preSerialized.push_back(SGNUP_CODE);
    j["response"] = resp.status;

    helper = intToBytes(j.dump().length());
    preSerialized.insert(preSerialized.end(), helper.begin(), helper.end());

    std::string jsonString = j.dump();
    for (int i = 0; i < jsonString.length(); i++)
    {
        preSerialized.push_back(jsonString[i]);
    }

    return preSerialized;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse resp)
{
    json j;
    std::vector<unsigned char> preSerialized;
    std::vector<unsigned char> helper;

    preSerialized.push_back(CREATEROOM_CODE);
    j["response"] = resp.status;

    helper = intToBytes(j.dump().length());
    preSerialized.insert(preSerialized.end(), helper.begin(), helper.end());

    std::string jsonString = j.dump();
    for (int i = 0; i < jsonString.length(); i++)
    {
        preSerialized.push_back(jsonString[i]);
    }

    return preSerialized;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse resp)
{
    json j;
    std::vector<unsigned char> preSerialized;
    std::vector<unsigned char> helper;

    preSerialized.push_back(JOINROOM_CODE);
    j["response"] = resp.status;

    helper = intToBytes(j.dump().length());
    preSerialized.insert(preSerialized.end(), helper.begin(), helper.end());

    std::string jsonString = j.dump();
    for (int i = 0; i < jsonString.length(); i++)
    {
        preSerialized.push_back(jsonString[i]);
    }

    return preSerialized;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse resp)
{
    std::vector<unsigned char> preSerialized;
    std::vector<unsigned char> helper;

    preSerialized.push_back(GETPLAYERS_CODE);
    std::string players = "";
    for (int i = 0; i < resp.players.size(); i++)
    {
        players.append(resp.players[i]);
        players.append(", ");
    }
    if (players.length() > 2)
        players.erase(players.length() - 2);

    helper = intToBytes(players.length());
    preSerialized.insert(preSerialized.end(), helper.begin(), helper.end());

    for (int i = 0; i < players.length(); i++)
    {
        preSerialized.push_back(players[i]);
    }

    return preSerialized;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse resp)
{
    std::vector<unsigned char> preSerialized;
    std::vector<unsigned char> helper;

    preSerialized.push_back(GETROOMS_CODE);
    std::string rooms = "";
    for (int i = 0; i < resp.rooms.size(); i++)
    {
        rooms.append(resp.rooms[i].name);
        rooms.append(", ");
        rooms.append(std::to_string(resp.rooms[i].id));
        rooms.append(", ");
    }
    if (rooms.length() > 2)
        rooms.erase(rooms.length() - 2);

    helper = intToBytes(rooms.length());
    preSerialized.insert(preSerialized.end(), helper.begin(), helper.end());

    for (int i = 0; i < rooms.length(); i++)
    {
        preSerialized.push_back(rooms[i]);
    }

    return preSerialized;
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse resp)
{
    json j;
    std::vector<unsigned char> preSerialized;
    std::vector<unsigned char> helper;

    preSerialized.push_back(LOGOUT_CODE);
    j["response"] = resp.status;

    helper = intToBytes(j.dump().length());
    preSerialized.insert(preSerialized.end(), helper.begin(), helper.end());

    std::string jsonString = j.dump();
    for (int i = 0; i < jsonString.length(); i++)
    {
        preSerialized.push_back(jsonString[i]);
    }

    return preSerialized;
}

std::vector<unsigned char> JsonResponsePacketSerializer::intToBytes(int paramInt)
{
    std::vector<unsigned char> arrayOfByte(4);
    for (int i = 0; i < 4; i++)
        arrayOfByte[3 - i] = (paramInt >> (i * 8));
    return arrayOfByte;
}
