#pragma once
#include <vector>

class IRequestHandler;

class RequestResult
{
public:
	RequestResult();
	~RequestResult();
	std::vector<unsigned char> buffer;
	IRequestHandler* newHandler;
};

