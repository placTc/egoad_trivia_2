#include "LoginManager.h"

void LoginManager::signup(std::string username, std::string password, std::string email, std::string address, std::string phoneNumber, std::string birthDate)
{
	if (!m_database->doesUserExist(username))
	{
		m_database->addNewUser(username, password, email, address, phoneNumber, birthDate);
	}
}

void LoginManager::login(std::string username, std::string password)
{
	bool loggedIn = false;

	for (int i = 0; i < m_loggedUsers.size(); i++)
	{
		if (m_loggedUsers[i].getUsername() == username)
		{
			loggedIn = true;
		}
	}

	if (!loggedIn)
	{
		if (m_database->doesPasswordMatch(username, password))
		{
			LoggedUser userLoggedIn(username);
			this->m_loggedUsers.push_back(userLoggedIn);
		}
		else
		{
			std::cout << "Failed to log in" << std::endl;
		}
	}
}

void LoginManager::logout(std::string username)
{
	std::vector<LoggedUser>::iterator it;
	for (it = m_loggedUsers.begin(); it != m_loggedUsers.end(); ++it)
	{
		if (it->getUsername() == username)
		{
			it  = m_loggedUsers.erase(it);
			break;
		}
	}
}

bool LoginManager::checkUserLoggedIn(std::string username)
{
	for (LoggedUser& user : m_loggedUsers)
	{
		if (username == user.getUsername())
			return true;
	}
	return false;
}

LoginManager::LoginManager(IDatabase* db)
{
	m_database = db;
}
