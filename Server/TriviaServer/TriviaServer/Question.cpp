#include "Question.h"

Question::Question(string question, std::vector<string> possibleAnswers, int correct_ans_id):
	m_question(question), m_possibleAnswers(possibleAnswers), m_correct_ans_id(correct_ans_id)
{
}

Question::Question():
	m_question(), m_possibleAnswers(), m_correct_ans_id()
{
}

Question::~Question()
{
}

void Question::set_question(string question)
{
	m_question = question;
}

void Question::set_possibleAnswers(std::vector<string> possibleAnswers)
{
	m_possibleAnswers = possibleAnswers;
}

void Question::set_correct_ans_id(int id)
{
	m_correct_ans_id = id;
}

string Question::getQuestion()
{
	return m_question;
}

std::vector<string> Question::getPossibleAnswers()
{
	return m_possibleAnswers;
}

string Question::getCorrentAnswer()
{
	return m_possibleAnswers[m_correct_ans_id-1];
}

int Question::getCorrectAnsId()
{
	return this->m_correct_ans_id;
}
