#pragma once
#include <regex>
#include <string>
#include<iostream>

class varChecker
{
public:
    static bool is_password_valid(const std::string password)
	{
		std::regex pattern("^(?=.*[A-Z])(?=.*[a-z])(?=.*\\d)(?=.*[@$!%*#?&])[A-Za-z\\d@$!%*#?&]{8,}$");
		return std::regex_match(password, pattern);
	}
	static bool is_email_valid(const std::string email)
	{
		std::regex pattern("^([a-zA-Z0-9]+)@([a-zA-Z0-9]+)(\\.([a-zA-Z0-9]+))+$");
		return std::regex_match(email, pattern);
	}
	static bool is_address_valid(const std::string address)
	{
		std::regex pattern("^\\(([a-zA-Z-]+),([0-9]+),([a-zA-Z-]+)\\)$");
		return std::regex_match(address, pattern);
	}
	static bool is_phoneNumber_valid(const std::string phoneNumber)
	{
		std::regex pattern("^0[0-9]{1,2}-[0-9]{7,7}$");
		return std::regex_match(phoneNumber, pattern);
	}
	static bool is_date_valid(const std::string date)
	{
		std::regex pattern("^[0-9]{2,2}/[0-9]{2,2}/[0-9]{4,4}$");
		if (std::regex_match(date, pattern))
		{
			std::string day_s = "";
			day_s.push_back(date[0]);
			day_s.push_back(date[1]);
			std::string month_s = "";
			month_s.push_back(date[3]);
			month_s.push_back(date[4]);
			std::string year_s = "";
			year_s.push_back(date[6]);
			year_s.push_back(date[7]);
			year_s.push_back(date[8]);
			year_s.push_back(date[9]);
			return isValidDate(std::stoi(day_s), std::stoi(month_s), std::stoi(year_s));
		}

		return false;
	}

private:
	static bool isLeap(int year)
	{
		return (((year % 4 == 0) &&
			(year % 100 != 0)) ||
			(year % 400 == 0));
	}

	static bool isValidDate(int d, int m, int y)
	{
		if (m < 1 || m > 12)
			return false;
		if (d < 1 || d > 31)
			return false;


		if (m == 2)
		{
			if (isLeap(y))
				return (d <= 29);
			else
				return (d <= 28);
		}

		if (m == 4 || m == 6 ||
			m == 9 || m == 11)
			return (d <= 30);

		return true;
	}
    // Disallow creating an instance of this object
    varChecker() {}
};
