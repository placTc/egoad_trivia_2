#pragma once
#define PORT 42069
#include "Helper.h"
#include "IRequestHandler.h"
#include "LoginRequest.h"
#include "SignupRequest.h"
#include "LoginRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "LoggedUser.h"
#include "LoginManager.h"
#include "Question.h"
#include "SqliteDatabase.h"
#include <map>
#include <mutex>
class Communicator
{
private:
	SOCKET m_serverSocket; 
	std::map<SOCKET, IRequestHandler*> m_clients;
	void bindAndListen();
	void handleNewClient(SOCKET sock, IRequestHandler* handler);

	std::mutex socketMtx;
	std::mutex mapMtx;
	std::mutex iostreamMtx;
public:
	Communicator();
	~Communicator() noexcept(false);
	void startHandleRequests();
};