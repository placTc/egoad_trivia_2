#pragma once

#include <string>
#include <List>
#include <iostream>
#include "Question.h"


using std::string;


class IDatabase
{
public:
	virtual bool doesUserExist(string username) = 0;
	virtual bool doesPasswordMatch(string, string) = 0;
	virtual void addNewUser(string username, string password, string email, string address, string phoneNumber, string birthDate) = 0;

	virtual std::list<Question> getQuestions(int) = 0;
	//virtual float getPlayerAverageAnswerTime(string) = 0;
	//virtual int getNumOfCorrectAnswers(string) = 0;
	//virtual int getNumOfTotalAnswers(string) = 0;
	//virtual int getNumOfPlayerGames(string) = 0;

	virtual int getSecurityKey(string) = 0;
};

