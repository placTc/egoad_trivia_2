#pragma once
#include <vector>
#include <string>
class GetPlayersInRoomResponse
{
public:
	std::vector<std::string> players;
	unsigned int status;
	GetPlayersInRoomResponse();
	~GetPlayersInRoomResponse();
};

