#pragma comment(lib, "Ws2_32.lib")
#pragma once
#include "Helper.h"
#include "Communicator.h"
#include <WinSock2.h>
#include <Windows.h>
#include <mutex>
#include <algorithm>

class Server
{
public:
	Server();
	~Server();
	void run();

private:
	Communicator m_communicator;
};

