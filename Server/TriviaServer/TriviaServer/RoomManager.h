#pragma once
#include "Room.h"
#include <map>

class RoomManager
{

public:
	static std::map<int, Room> m_rooms;
	void createRoom(LoggedUser user, RoomData data);
	void deleteRoom(int id);
	bool getRoomState(int id);
	std::vector<RoomData> getRooms();
};

