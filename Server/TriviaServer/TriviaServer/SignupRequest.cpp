#include "SignupRequest.h"

SignupRequest::SignupRequest(std::string username, std::string password, std::string email, std::string address, std::string phoneNumber, std::string birthDate) :
	username(username), password(password), email(email), address(address), phoneNumber(phoneNumber), birthDate(birthDate)
{
}

SignupRequest::~SignupRequest()
{
}
