#pragma once
#include <string>
class SignupRequest
{
public:
	SignupRequest(std::string, std::string, std::string, std::string, std::string, std::string);
	~SignupRequest();

	std::string username;
	std::string password;
	std::string email;
	std::string address;
	std::string phoneNumber;
	std::string birthDate;
};


