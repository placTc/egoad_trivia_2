#pragma once

#include "IDatabase.h"
#include "sqlite3.h"
#include <io.h>
#include <fstream>
#include <sstream>

#define DB_NAME "Trivia_db.db"
#define QuestionsFile "QuestionsToLoad.txt"

class SqliteDatabase :
	public IDatabase
{
public:
	bool open();
	void close();
	void clear();

	bool doesUserExist(string username);
	bool doesPasswordMatch(string username, string password);
	void addNewUser(string username, string password, string email, string address, string phoneNumber, string birthDate);

	std::list<Question> getQuestions(int);

	int getSecurityKey(string);
private:
	static std::vector<string> split(string str, string token);
	static void insertQuestions(string fileName, sqlite3* db);
	static int get_user_callback(void* data, int argc, char** argv, char** azColName);
	static int get_password_callback(void* data, int argc, char** argv, char** azColName);
	static int get_questions_callback(void* data, int argc, char** argv, char** azColName);

	sqlite3* _db;
};

