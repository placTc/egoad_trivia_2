#include "RoomManager.h"

void RoomManager::createRoom(LoggedUser user, RoomData data)
{
	Room room(data);
	room.addUser(user);
	int id = 1;

	if(!m_rooms.empty())
	{
		id = m_rooms.rbegin()->first + 1;
	}
	m_rooms.emplace(id, room);
}

void RoomManager::deleteRoom(int id)
{
	m_rooms.erase(id);
}

bool RoomManager::getRoomState(int id)
{
	return m_rooms[id].getState();
}

std::vector<RoomData> RoomManager::getRooms()
{
	std::vector<RoomData> roomdata;
	std::map<int, Room>::iterator it = m_rooms.begin();
	while (it != m_rooms.end())
	{
		roomdata.push_back(it->second.getData());
		it++;
	}

	return roomdata;
}
