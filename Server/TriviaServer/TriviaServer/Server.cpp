#include "Server.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <set>
#include <fstream>
#include <sstream>

Server::Server():
	m_communicator()
{
}

Server::~Server()
{
}

void Server::run()
{
	std::thread t_connector(&Communicator::startHandleRequests, std::ref(this->m_communicator));
	t_connector.detach();
	
	bool run = true;
	std::string inputString;

	while (run)
	{
		std::cin >> inputString;
		if (inputString == "EXIT")
		{
			run = false;
		}
	}
}

