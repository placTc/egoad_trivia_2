#pragma once
#include <string>
class LoginRequest
{
public:
	LoginRequest(std::string, std::string);
	~LoginRequest();

	std::string username;
	std::string password;
};

