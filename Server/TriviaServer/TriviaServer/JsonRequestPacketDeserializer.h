#pragma once
#include "LoginRequest.h"
#include "SignupRequest.h"
#include "GetPlayersInRoomRequest.h"
#include "CreateRoomRequest.h"
#include "JoinRoomRequest.h"
#include "json.hpp"

using json = nlohmann::json;
class JsonRequestPacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(std::vector<unsigned char> buffer, int length);
	static SignupRequest deserializeSignupRequest(std::vector<unsigned char> buffer, int length);
	static GetPlayersInRoomRequest deserializeGetPlayersInRoomRequest(std::vector<unsigned char> buffer, int length);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer, int length);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer, int length);
};

