#include "RequestHandlerFactory.h"

IRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler();
}

IRequestHandler* RequestHandlerFactory::createMenuRequestHandler(std::string user)
{
	return new MenuRequestHandler(user);
}
