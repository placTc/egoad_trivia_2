#include "LoginRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestResult.h"
#include "SqliteDatabase.h"
#include "varChecker.h"

LoginRequestHandler::LoginRequestHandler()
{
}

LoginRequestHandler::~LoginRequestHandler()
{
}

bool LoginRequestHandler::isReqeustRelevant(RequestInfo ri)
{
    if (ri.id != LOGIN_CODE && ri.id != SGNUP_CODE)
    {
        return false;
    }
    return true;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo ri)
{
	std::string username = "";
	SqliteDatabase sqlDatabase;
	sqlDatabase.open();
	LoginManager loginManager(&sqlDatabase);

    RequestResult rr;
	
	if (ri.id == SGNUP_CODE) // signup handling
	{
		SignupRequest singupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(ri.buffer, ri.buffer.size());
		if (!sqlDatabase.doesUserExist(singupRequest.username) && varChecker::is_password_valid(singupRequest.password) && varChecker::is_email_valid(singupRequest.email) && varChecker::is_address_valid(singupRequest.address) && varChecker::is_phoneNumber_valid(singupRequest.phoneNumber) && varChecker::is_date_valid(singupRequest.birthDate)) // check for the existance of the user (user has to not exist to sign up)// check for the existance of the user (user has to not exist to sign up)
		{
			username = singupRequest.username; // saving username for later
			loginManager.signup(singupRequest.username, singupRequest.password, singupRequest.email, singupRequest.address, singupRequest.phoneNumber, singupRequest.birthDate); // signup and login

			// return positive response to the client
			SignupResponse resp;
			resp.status = 1;
			std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(resp);
			rr.buffer = buf;

			rr.newHandler = RequestHandlerFactory::createLoginRequestHandler(); // swap handler for the next one
		}
		else
		{
			ErrorResponse resp;
			resp.message = "Sign up failure";
			std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(resp);
			rr.buffer = buf;
			rr.newHandler = RequestHandlerFactory::createLoginRequestHandler();
		}
	}
	else if (ri.id == LOGIN_CODE)
	{
		// login request handling
		LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(ri.buffer, ri.buffer.size());
		if (sqlDatabase.doesUserExist(loginRequest.username) && sqlDatabase.doesPasswordMatch(loginRequest.username, loginRequest.password)) // check for existance of the user (user has to exist)
		{
			if (!loginManager.checkUserLoggedIn(loginRequest.username))
			{
				// logging in the user
				username = loginRequest.username;
				loginManager.login(loginRequest.username, loginRequest.password);

				// sending positive response to the client after login
				LoginResponse resp;
				resp.status = 1;
				std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(resp);
				rr.buffer = buf;

				rr.newHandler = RequestHandlerFactory::createMenuRequestHandler(username); // swap handler for the next one
			}
			else
			{
				// error response in case of failure
				ErrorResponse resp;
				resp.message = "User already logged in";
				std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(resp);
				rr.buffer = buf;
				rr.newHandler = RequestHandlerFactory::createLoginRequestHandler();
			}
		}
		else
		{
			// error response in case of failure
			ErrorResponse resp;
			resp.message = "Invalid username or password";
			std::vector<unsigned char> buf = JsonResponsePacketSerializer::serializeResponse(resp);
			rr.buffer = buf;
			rr.newHandler = RequestHandlerFactory::createLoginRequestHandler();
		}
	}
	sqlDatabase.close();

	return rr;
}
