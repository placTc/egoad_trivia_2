#pragma once

#include "IDatabase.h"
#include "LoggedUser.h"
#include <vector>

class LoginManager
{
private:
	IDatabase* m_database;
	static std::vector<LoggedUser> m_loggedUsers;

public:
	void signup(std::string user, std::string pass, std::string email, std::string address, std::string phoneNumber, std::string birthDate);
	void login(std::string username, std::string password);
	void logout(std::string username);
	bool checkUserLoggedIn(std::string username);
	LoginManager(IDatabase* db);
};

