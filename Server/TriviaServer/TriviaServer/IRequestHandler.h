#pragma once
#include "IRequestHandler.h"
#include "RequestInfo.h"
#include "LoginManager.h"

class RequestResult;

class IRequestHandler
{
public:
	IRequestHandler();
	~IRequestHandler();
	virtual bool isReqeustRelevant(RequestInfo ri) = 0;
	virtual RequestResult handleRequest(RequestInfo ri) = 0;
};

