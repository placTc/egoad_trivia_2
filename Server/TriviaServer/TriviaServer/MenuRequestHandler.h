#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include <mutex>

#define JOINROOM_CODE 'J'
#define CREATEROOM_CODE 'C'
#define GETPLAYERS_CODE 'P'
#define LOGOUT_CODE 'O'
#define GETROOMS_CODE 'R'

class MenuRequestHandler :
    public IRequestHandler
{
private:
    LoggedUser m_user;
    std::mutex roomsMtx;
public:
    MenuRequestHandler(std::string user);
    ~MenuRequestHandler();
    bool isReqeustRelevant(RequestInfo ri);
    RequestResult handleRequest(RequestInfo ri);

};

