#include "Room.h"

Room::Room()
{
	this->m_metadata = RoomData(0, "", 0, 0, 0, false);
}

Room::Room(RoomData data):
	m_metadata(data), m_users(std::vector<LoggedUser>())
{
}

Room::~Room()
{
}

void Room::addUser(LoggedUser user)
{	
	bool userExists = false;
	for (int i = 0; i < m_users.size(); i++)
	{
		if (m_users[i].getUsername() == user.getUsername())
		{
			userExists = true;
		}
	}
	if(!userExists)
		m_users.push_back(user);
}

void Room::removeUser(LoggedUser user)
{
	for (int i = 0; i < m_users.size(); i++)
	{
		if (m_users[i].getUsername() == user.getUsername())
		{
			m_users.erase(m_users.begin() + i);
		}
	}
}

std::vector<std::string> Room::getAllUsers()
{
	std::vector<std::string> users;
	for (int i = 0; i < m_users.size(); i++)
	{
		users.push_back(m_users[i].getUsername());
	}

	return users;
}

bool Room::getState()
{
	return m_metadata.isActive;
}

RoomData Room::getData()
{
	return m_metadata;
}
