﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace gui_client_igoad
{
    public class Helper
    {
        public static int trollCounter = 0;
        public const string ZERO_LENGTH = "\u0000\u0000\u0000\u0000";
        public const char LOGIN_CODE = 'L';
        public const char SGNUP_CODE = 'S';
        public const char GETROOMS_CODE = 'R';
        public const char GETPLAYERS_CODE = 'P';
        public const char JOINROOM_CODE = 'J';
        public const char CREATEROOM_CODE = 'C';
        public const char LOGOUT_CODE = 'O';
        public static string loggedUserName = string.Empty;
        public static string room_joined = string.Empty;
        public static int roomID = 0;
        public static int maxPlayers = 0;
        public static int numberOfQuestions = 0;
        public static int timeForQuestion = 0;
        public Helper()
        {

        }
        public static string[] GetUsersInRoom(int id)
        {
            try
            {
                Networking.SendData(SerializationFunctions.SerializeGetPlayersInRoomRequest(id));
                string response = Networking.ReceiveData();
                string[] players = SerializationFunctions.ParseGetPlayers(response);

                return players;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
                return new string[0];
            }
        }

        public static void notImplemented()
        {
            MessageBox.Show("This feature is not implemented yet!", "Not implemented", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
