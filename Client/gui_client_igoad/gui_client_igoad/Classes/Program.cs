﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;

namespace gui_client_igoad
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new mainMenu());

            try
            {
                List<byte> bytes = new List<byte>();
                bytes.Add(Convert.ToByte(Helper.LOGOUT_CODE));
                bytes.AddRange(Encoding.ASCII.GetBytes(Helper.ZERO_LENGTH));
                Networking.SendData(bytes);
            }
            catch(Exception ex) { }
        }
    }
}
