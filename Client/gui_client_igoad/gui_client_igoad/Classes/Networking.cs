﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Windows.Forms;
using System.IO;

namespace gui_client_igoad
{
    class Networking
    {
        private static int port = 0;
        private static string ip = "";
        private static TcpClient pipeline;
        private static NetworkStream stream;

        private static byte[] data = new byte[1024];
        private static string stringData = String.Empty;

        public static string ReceiveData()
        {
            int lengthRead = 0;
            try
            {
                lengthRead = stream.Read(data, 0, data.Length);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }

            if (lengthRead > 0)
            {
                stringData = Encoding.ASCII.GetString(data, 0, lengthRead);
            }

            return stringData;
        }

        public static bool SendData(List<byte> data)
        {
            try
            {
                stream.Write(data.ToArray(), 0, data.Count);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
                return false;
            }
        }

        public static void networkInit()
        {
            try
            {
                string settingsFile = @"settings.txt"; // found in the /bin/Debug/ folder
                IEnumerable<string> lines = File.ReadLines(settingsFile);
                port = int.Parse(lines.ElementAt(1).Split('=')[1]);
                ip = lines.ElementAt(0).Split('=')[1];
                pipeline = new TcpClient(ip, port);
                stream = pipeline.GetStream();
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: Could not reach server", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
        }
    }
}
