﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;

namespace gui_client_igoad
{
    class SerializationFunctions
    {
        public static List<byte> SerializeLoginRequest(string username, string password)
        {
            User usr = new User(username, password);
            string serialized = JsonSerializer.Serialize(usr);
            
            byte[] bytes = BitConverter.GetBytes(serialized.Length);
            Array.Reverse(bytes);
            List<byte> bytes2 = new List<byte>();
            bytes2.Add(Convert.ToByte(Helper.LOGIN_CODE));
            bytes2.AddRange(bytes);
            bytes2.AddRange(Encoding.ASCII.GetBytes(serialized));

            return bytes2;
        }

        public static List<byte> SerializeSignupRequest(string username, string password, string email, string address, string phoneNumber, string birthDate)
        {
            SignupUser usr = new SignupUser(username, password, email, address, phoneNumber, birthDate);
            string serialized = JsonSerializer.Serialize(usr);
            byte[] bytes = BitConverter.GetBytes(serialized.Length);
            Array.Reverse(bytes);
            List<byte> bytes2 = new List<byte>();
            bytes2.Add(Convert.ToByte(Helper.SGNUP_CODE));
            bytes2.AddRange(bytes);
            bytes2.AddRange(Encoding.ASCII.GetBytes(serialized));

            return bytes2;
        }

        public static List<byte> SerializeJoinRoomRequest(int id)
        {
            RoomID jr = new RoomID(id);
            string serialized = JsonSerializer.Serialize(jr);
            byte[] bytes = BitConverter.GetBytes(serialized.Length);
            Array.Reverse(bytes);
            List<byte> bytes2 = new List<byte>();
            bytes2.Add(Convert.ToByte(Helper.JOINROOM_CODE));
            bytes2.AddRange(bytes);
            bytes2.AddRange(Encoding.ASCII.GetBytes(serialized));

            return bytes2;
        }

        public static List<byte> SerializeCreateRoomRequest(string name, int players, int questions, int time)
        {
            Room room = new Room(name, players, questions, time);
            string serialized = JsonSerializer.Serialize(room);
            byte[] bytes = BitConverter.GetBytes(serialized.Length);
            Array.Reverse(bytes);
            List<byte> bytes2 = new List<byte>();
            bytes2.Add(Convert.ToByte(Helper.CREATEROOM_CODE));
            bytes2.AddRange(bytes);
            bytes2.AddRange(Encoding.ASCII.GetBytes(serialized));

            return bytes2;
        }

        public static List<byte> SerializeGetPlayersInRoomRequest(int id)
        {
            RoomID jr = new RoomID(id);
            string serialized = JsonSerializer.Serialize(jr);
            byte[] bytes = BitConverter.GetBytes(serialized.Length);
            Array.Reverse(bytes);
            List<byte> bytes2 = new List<byte>();
            bytes2.Add(Convert.ToByte(Helper.GETPLAYERS_CODE));
            bytes2.AddRange(bytes);
            bytes2.AddRange(Encoding.ASCII.GetBytes(serialized));

            return bytes2;
        }

        public static bool ParseGenericResponse(string response)
        {
            if(response.Length > 5)
            {
                string str = response.Substring(5);
                if (response[0] == Helper.LOGIN_CODE || response[0] == Helper.SGNUP_CODE || response[0] == Helper.LOGOUT_CODE || response[0] == Helper.JOINROOM_CODE)
                {
                    NonErrorResponse resp = JsonSerializer.Deserialize<NonErrorResponse>(str);
                    if (resp.response == 1)
                        return true;
                    else
                        return false;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public static string ParseErrorResponse(string response)
        {
            if(response.Length > 5)
            {
                string str = response.Substring(5);
                ErrorResponse resp = JsonSerializer.Deserialize<ErrorResponse>(str);
                return resp.response;
            }
            return "Server error";
        }

        public static int ParseCreateRoomResponse(string response)
        {
            if (response.Length > 5)
            {
                string str = response.Substring(5);
                if (response[0] == Helper.CREATEROOM_CODE)
                {
                    NonErrorResponse resp = JsonSerializer.Deserialize<NonErrorResponse>(str);
                    return resp.response;
                }
            }
            return -1;
        }

        public static Tuple<List<string>, List<int>> ParseGetRooms(string response)
        {
            List<string> roomNames = new List<string>();
            List<int> roomIDs = new List<int>();
            if(response.Length > 5)
            {
                string[] responseParsed = response.Substring(5).Split(new[] { ", " }, StringSplitOptions.None);
                for (int i = 0; i < responseParsed.Length; i += 2)
                {
                    roomNames.Add(responseParsed[i]);
                    roomIDs.Add(int.Parse(responseParsed[i + 1]));
                }

            }
            return new Tuple<List<string>, List<int>>(roomNames, roomIDs);
        }

        public static string[] ParseGetPlayers(string response)
        {
            if(response.Length > 5)
            {
                string[] responseParsed = response.Substring(5).Split(new[] { ", " }, StringSplitOptions.None);
                return responseParsed;
            }

            return new string[0];
        }
    }

    class User
    {
        public string username { get; set; }
        public string password { get; set; }
        public User(string username, string password)
        {
            this.username = username;
            this.password = password;
        }
    }

    class SignupUser
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string phoneNumber { get; set; }
        public string birthDate { get; set; }

        public SignupUser(string username, string password, string email, string address, string phoneNumber, string birthDate)
        {
            this.username = username;
            this.password = password;
            this.email = email;
            this.address = address;
            this.phoneNumber = phoneNumber;
            this.birthDate = birthDate;
        }
    }

    class Room
    {
        public string roomName { get; set; }
        public int maxUsers { get; set; }
        public int questionCount { get; set; }
        public int answerTime { get; set; }

        public Room(string name, int players, int questions, int time)
        {
            this.roomName = name;
            this.maxUsers = players;
            this.questionCount = questions;
            this.answerTime = time;
        }
    }

    class RoomID
    {
        public int roomID { get; set; }
        public RoomID(int id)
        {
            this.roomID = id;
        }
    }

    class NonErrorResponse
    {
        public int response { get; set; }

        public NonErrorResponse(int response)
        {
            this.response = response;
        }
    }

    class ErrorResponse
    {
        public string response { get; set; }

        public ErrorResponse(string response)
        {
            this.response = response;
        }
    }
}
