﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace gui_client_igoad
{
    public partial class mainMenu : Form
    {
        static bool is_signed_in = false;

        public mainMenu()
        {
            InitializeComponent();
            Networking.networkInit();
            this.FormClosed += new FormClosedEventHandler(MainMenuClose);
            this.join_room_butt.Enabled = false;
            this.create_room_butt.Enabled = false;
        }

        private void Sign_up_button_Click(object sender, EventArgs e)
        {
            if (is_signed_in) //supposed to also sign you out
            {
                try
                {
                    List<byte> bytes = new List<byte>();
                    bytes.Add(Convert.ToByte(Helper.LOGOUT_CODE));
                    bytes.AddRange(Encoding.ASCII.GetBytes(Helper.ZERO_LENGTH));
                    Networking.SendData(bytes);
                    if (SerializationFunctions.ParseGenericResponse(Networking.ReceiveData()))
                    {
                        is_signed_in = false;
                        Sign_up_button.Text = "Sign Up";
                        invalid_message.ForeColor = sign_in_panel.BackColor;
                        sign_in_panel.Visible = true;
                        Welcome_lable.Visible = false;
                        this.join_room_butt.Enabled = false;
                        this.create_room_butt.Enabled = false;
                        Helper.loggedUserName = string.Empty;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Environment.Exit(1);
                }
            }
            else
            {
                Sign_Up frm = new Sign_Up();
                frm.Location = this.Location;
                frm.StartPosition = FormStartPosition.Manual;
                frm.FormClosing += delegate { this.Show(); };
                frm.Show();
                this.Hide();
            }
        }

        private void sign_in_butt_Click(object sender, EventArgs e)
        {
            string username = textBox_username.Text;
            string password = textBox_password.Text;
            string response = String.Empty;
            bool responseSuccess = false;

            List<byte> serialized = SerializationFunctions.SerializeLoginRequest(username, password);
            try
            {
                if (Networking.SendData(serialized))
                {
                    response = Networking.ReceiveData();
                    responseSuccess = SerializationFunctions.ParseGenericResponse(response);
                }
                if ((username.Length == 0 && password.Length == 0) || !responseSuccess) //supposed to get if valid from server
                {
                    string message = SerializationFunctions.ParseErrorResponse(response);
                    invalid_message.ForeColor = System.Drawing.Color.Red;
                    invalid_message.Text = message;
                }
                else
                {
                    is_signed_in = true;
                    Sign_up_button.Text = "Sign Out";
                    invalid_message.ForeColor = sign_in_panel.BackColor;
                    sign_in_panel.Visible = false;
                    Welcome_lable.Visible = true;
                    Welcome_lable.Text = "Welcome " + username + "!";
                    this.join_room_butt.Enabled = true;
                    this.create_room_butt.Enabled = true;
                    Helper.loggedUserName = username;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }



        }

        private void textBox_username_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                textBox_password.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void textBox_password_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                sign_in_butt.PerformClick();
            }
        }

        private void eye_butt_Click(object sender, EventArgs e)
        {
            if (eye_butt.BackgroundImage.Height == 133) //133 is not_eye and 132 is eye
            {
                eye_butt.BackgroundImage = Properties.Resources.eye_butt;
                textBox_password.UseSystemPasswordChar = false;

            }
            else if (eye_butt.BackgroundImage.Height == 132) //133 is not_eye and 132 is eye
            {
                eye_butt.BackgroundImage = Properties.Resources.not_eye_butt;
                textBox_password.UseSystemPasswordChar = true;
            }
        }

        private void troll_butt_MouseHover(object sender, EventArgs e)
        {
            
            string[] message = { "You've gotten trolled!!!!", "We do be doing a bit of trolling", "sussy!!!!!!!!!", "peepeepoopoo check", "steave jobs", "s(he) be(lie)ve(d)", "your computer has been hacked", "option 7 filler", "sussy baka", "option 9 filler" };
            string title = "Loser";
            Random r = new Random();
            int randomNumber = r.Next(0, message.Length);
            if(randomNumber == 7)
            {
                MessageBox.Show(String.Format("you've gotten trolled {0} times, PLUS ONE", Helper.trollCounter), title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if(randomNumber == 9)
            {
                randomNumber = r.Next(0, 2);
                if(randomNumber == 0)
                {
                    Process.Start("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
                    MessageBox.Show("rickrolled", title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("i could have rickrolled you", title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
                MessageBox.Show(message[randomNumber], title, MessageBoxButtons.OK, MessageBoxIcon.Warning);

            Helper.trollCounter++;
        }

        private void MainMenuClose(object sender, EventArgs e)
        {
            if(is_signed_in)
            {
                List<byte> bytes = new List<byte>();
                bytes.Add(Convert.ToByte(Helper.LOGOUT_CODE));
                bytes.AddRange(Encoding.ASCII.GetBytes(Helper.ZERO_LENGTH));
                Networking.SendData(bytes);
            }
        }

        private void join_room_butt_Click(object sender, EventArgs e)
        {
            join_room frm = new join_room();
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.Show();
            this.Hide();
        }

        private void create_room_butt_Click(object sender, EventArgs e)
        {
            create_room frm = new create_room();
            frm.Location = this.Location;
            frm.StartPosition = FormStartPosition.Manual;
            frm.FormClosing += delegate { this.Show(); };
            frm.Show();
            this.Hide();
        }
    }
}
