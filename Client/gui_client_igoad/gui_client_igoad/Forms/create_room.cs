﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace gui_client_igoad
{
    public partial class create_room : Form
    {
        private Panel panel1;
        private TextBox time_for_questions_textBox;
        private TextBox num_of_questions_textBox;
        private TextBox num_of_players_textBox;
        private TextBox room_name_textBox;
        private Button create_room_button;
        private Label time_for_question_label;
        private Label num_of_questions_label;
        private Label number_of_players_label;
        private Label room_name_label;
        private Button back_button;
        private Button IT_TRIVIA;
        private Label loggedName;

        public create_room()
        {
            InitializeComponent();
            loggedName.Text = Helper.loggedUserName;
        }

        private bool CreateRoom(string name, int players, int questions, int time)
        {
            try
            {
                if (Networking.SendData(SerializationFunctions.SerializeCreateRoomRequest(name, players, questions, time)))
                {
                    string response = Networking.ReceiveData();
                    Helper.roomID = SerializationFunctions.ParseCreateRoomResponse(response);
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
                return false;
            }
        }


        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(create_room));
            this.IT_TRIVIA = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.time_for_questions_textBox = new System.Windows.Forms.TextBox();
            this.num_of_questions_textBox = new System.Windows.Forms.TextBox();
            this.num_of_players_textBox = new System.Windows.Forms.TextBox();
            this.room_name_textBox = new System.Windows.Forms.TextBox();
            this.create_room_button = new System.Windows.Forms.Button();
            this.time_for_question_label = new System.Windows.Forms.Label();
            this.num_of_questions_label = new System.Windows.Forms.Label();
            this.number_of_players_label = new System.Windows.Forms.Label();
            this.room_name_label = new System.Windows.Forms.Label();
            this.back_button = new System.Windows.Forms.Button();
            this.loggedName = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // IT_TRIVIA
            // 
            this.IT_TRIVIA.BackgroundImage = global::gui_client_igoad.Properties.Resources.it_logo_new;
            this.IT_TRIVIA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IT_TRIVIA.FlatAppearance.BorderSize = 0;
            this.IT_TRIVIA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IT_TRIVIA.Location = new System.Drawing.Point(185, 0);
            this.IT_TRIVIA.Name = "IT_TRIVIA";
            this.IT_TRIVIA.Size = new System.Drawing.Size(264, 137);
            this.IT_TRIVIA.TabIndex = 14;
            this.IT_TRIVIA.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.time_for_questions_textBox);
            this.panel1.Controls.Add(this.num_of_questions_textBox);
            this.panel1.Controls.Add(this.num_of_players_textBox);
            this.panel1.Controls.Add(this.room_name_textBox);
            this.panel1.Controls.Add(this.create_room_button);
            this.panel1.Controls.Add(this.time_for_question_label);
            this.panel1.Controls.Add(this.num_of_questions_label);
            this.panel1.Controls.Add(this.number_of_players_label);
            this.panel1.Controls.Add(this.room_name_label);
            this.panel1.Location = new System.Drawing.Point(81, 192);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(463, 240);
            this.panel1.TabIndex = 18;
            // 
            // time_for_questions_textBox
            // 
            this.time_for_questions_textBox.Location = new System.Drawing.Point(209, 138);
            this.time_for_questions_textBox.Name = "time_for_questions_textBox";
            this.time_for_questions_textBox.Size = new System.Drawing.Size(53, 20);
            this.time_for_questions_textBox.TabIndex = 8;
            this.time_for_questions_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.time_for_questions_textBox_KeyPress);
            // 
            // num_of_questions_textBox
            // 
            this.num_of_questions_textBox.Location = new System.Drawing.Point(225, 100);
            this.num_of_questions_textBox.Name = "num_of_questions_textBox";
            this.num_of_questions_textBox.Size = new System.Drawing.Size(58, 20);
            this.num_of_questions_textBox.TabIndex = 7;
            this.num_of_questions_textBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.num_of_questions_textBox_KeyDown);
            // 
            // num_of_players_textBox
            // 
            this.num_of_players_textBox.Location = new System.Drawing.Point(198, 61);
            this.num_of_players_textBox.Name = "num_of_players_textBox";
            this.num_of_players_textBox.Size = new System.Drawing.Size(53, 20);
            this.num_of_players_textBox.TabIndex = 6;
            this.num_of_players_textBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.num_of_players_textBox_KeyDown);
            // 
            // room_name_textBox
            // 
            this.room_name_textBox.Location = new System.Drawing.Point(133, 26);
            this.room_name_textBox.Name = "room_name_textBox";
            this.room_name_textBox.Size = new System.Drawing.Size(165, 20);
            this.room_name_textBox.TabIndex = 5;
            this.room_name_textBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.room_name_textBox_KeyDown);
            // 
            // create_room_button
            // 
            this.create_room_button.BackColor = System.Drawing.Color.Honeydew;
            this.create_room_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.create_room_button.Location = new System.Drawing.Point(158, 191);
            this.create_room_button.Name = "create_room_button";
            this.create_room_button.Size = new System.Drawing.Size(151, 46);
            this.create_room_button.TabIndex = 4;
            this.create_room_button.Text = "Create Room";
            this.create_room_button.UseVisualStyleBackColor = false;
            this.create_room_button.Click += new System.EventHandler(this.create_room_button_Click);
            // 
            // time_for_question_label
            // 
            this.time_for_question_label.AutoSize = true;
            this.time_for_question_label.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.time_for_question_label.Location = new System.Drawing.Point(8, 131);
            this.time_for_question_label.Name = "time_for_question_label";
            this.time_for_question_label.Size = new System.Drawing.Size(209, 27);
            this.time_for_question_label.TabIndex = 3;
            this.time_for_question_label.Text = "Time For Questions: ";
            // 
            // num_of_questions_label
            // 
            this.num_of_questions_label.AutoSize = true;
            this.num_of_questions_label.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num_of_questions_label.Location = new System.Drawing.Point(8, 93);
            this.num_of_questions_label.Name = "num_of_questions_label";
            this.num_of_questions_label.Size = new System.Drawing.Size(225, 27);
            this.num_of_questions_label.TabIndex = 2;
            this.num_of_questions_label.Text = "Number of Questions: ";
            // 
            // number_of_players_label
            // 
            this.number_of_players_label.AutoSize = true;
            this.number_of_players_label.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.number_of_players_label.Location = new System.Drawing.Point(8, 54);
            this.number_of_players_label.Name = "number_of_players_label";
            this.number_of_players_label.Size = new System.Drawing.Size(201, 27);
            this.number_of_players_label.TabIndex = 1;
            this.number_of_players_label.Text = "Number of Players: ";
            // 
            // room_name_label
            // 
            this.room_name_label.AutoSize = true;
            this.room_name_label.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.room_name_label.Location = new System.Drawing.Point(8, 19);
            this.room_name_label.Name = "room_name_label";
            this.room_name_label.Size = new System.Drawing.Size(135, 27);
            this.room_name_label.TabIndex = 0;
            this.room_name_label.Text = "Room Name: ";
            // 
            // back_button
            // 
            this.back_button.BackColor = System.Drawing.Color.Honeydew;
            this.back_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.back_button.Location = new System.Drawing.Point(536, 572);
            this.back_button.Name = "back_button";
            this.back_button.Size = new System.Drawing.Size(76, 40);
            this.back_button.TabIndex = 19;
            this.back_button.Text = "Back";
            this.back_button.UseVisualStyleBackColor = false;
            this.back_button.Click += new System.EventHandler(this.back_button_Click_1);
            // 
            // loggedName
            // 
            this.loggedName.BackColor = System.Drawing.Color.Transparent;
            this.loggedName.Font = new System.Drawing.Font("Comic Sans MS", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loggedName.ForeColor = System.Drawing.Color.White;
            this.loggedName.Location = new System.Drawing.Point(12, 9);
            this.loggedName.Name = "loggedName";
            this.loggedName.Size = new System.Drawing.Size(145, 32);
            this.loggedName.TabIndex = 20;
            // 
            // create_room
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImage = global::gui_client_igoad.Properties.Resources.background3;
            this.ClientSize = new System.Drawing.Size(624, 624);
            this.Controls.Add(this.loggedName);
            this.Controls.Add(this.back_button);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.IT_TRIVIA);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "create_room";
            this.Text = "Create Room";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }


        private void back_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void back_button_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void create_room_button_Click(object sender, EventArgs e)
        {
            if(CreateRoom(this.room_name_textBox.Text, int.Parse(this.num_of_players_textBox.Text), int.Parse(this.num_of_questions_textBox.Text), int.Parse(this.time_for_questions_textBox.Text)))
            {
                Helper.room_joined = this.room_name_textBox.Text;
                Helper.maxPlayers = int.Parse(this.num_of_players_textBox.Text);
                Helper.numberOfQuestions = int.Parse(this.num_of_questions_textBox.Text);
                Helper.timeForQuestion = int.Parse(this.time_for_questions_textBox.Text);

                manage_room frm = new manage_room();
                frm.Location = this.Location;
                frm.StartPosition = FormStartPosition.Manual;
                frm.FormClosing += delegate { this.Show(); };
                frm.Show();
                this.Hide();
            }
            else
            {
                // idfk -Egor 2021-
            }
        }

        private void room_name_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                num_of_players_textBox.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void num_of_players_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                num_of_questions_textBox.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void num_of_questions_textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                time_for_questions_textBox.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void time_for_questions_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                create_room_button.PerformClick();
            }
        }
    }
}
