﻿namespace gui_client_igoad
{
    partial class waitForGame
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(waitForGame));
            this.IT_TRIVIA = new System.Windows.Forms.Button();
            this.room_name_label = new System.Windows.Forms.Label();
            this.currentPartticipantsTitleLabel = new System.Windows.Forms.Label();
            this.connectedUsers = new System.Windows.Forms.DataGridView();
            this.leaveRoomButton = new System.Windows.Forms.Button();
            this.loggedName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.connectedUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // IT_TRIVIA
            // 
            this.IT_TRIVIA.BackgroundImage = global::gui_client_igoad.Properties.Resources.it_logo_new;
            this.IT_TRIVIA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IT_TRIVIA.FlatAppearance.BorderSize = 0;
            this.IT_TRIVIA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IT_TRIVIA.Location = new System.Drawing.Point(296, 0);
            this.IT_TRIVIA.Name = "IT_TRIVIA";
            this.IT_TRIVIA.Size = new System.Drawing.Size(200, 93);
            this.IT_TRIVIA.TabIndex = 14;
            this.IT_TRIVIA.UseVisualStyleBackColor = true;
            // 
            // room_name_label
            // 
            this.room_name_label.BackColor = System.Drawing.Color.Transparent;
            this.room_name_label.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.room_name_label.ForeColor = System.Drawing.Color.White;
            this.room_name_label.Location = new System.Drawing.Point(238, 128);
            this.room_name_label.Name = "room_name_label";
            this.room_name_label.Size = new System.Drawing.Size(318, 47);
            this.room_name_label.TabIndex = 15;
            this.room_name_label.Text = "Room1";
            this.room_name_label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // currentPartticipantsTitleLabel
            // 
            this.currentPartticipantsTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.currentPartticipantsTitleLabel.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentPartticipantsTitleLabel.ForeColor = System.Drawing.Color.White;
            this.currentPartticipantsTitleLabel.Location = new System.Drawing.Point(246, 175);
            this.currentPartticipantsTitleLabel.Name = "currentPartticipantsTitleLabel";
            this.currentPartticipantsTitleLabel.Size = new System.Drawing.Size(300, 30);
            this.currentPartticipantsTitleLabel.TabIndex = 28;
            this.currentPartticipantsTitleLabel.Text = "Current Users Connected:";
            this.currentPartticipantsTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // connectedUsers
            // 
            this.connectedUsers.AllowUserToDeleteRows = false;
            this.connectedUsers.AllowUserToResizeRows = false;
            this.connectedUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.connectedUsers.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.connectedUsers.BackgroundColor = System.Drawing.Color.White;
            this.connectedUsers.ColumnHeadersHeight = 20;
            this.connectedUsers.ColumnHeadersVisible = false;
            this.connectedUsers.EnableHeadersVisualStyles = false;
            this.connectedUsers.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.connectedUsers.Location = new System.Drawing.Point(238, 208);
            this.connectedUsers.Name = "connectedUsers";
            this.connectedUsers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.connectedUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.connectedUsers.Size = new System.Drawing.Size(308, 123);
            this.connectedUsers.TabIndex = 29;
            // 
            // leaveRoomButton
            // 
            this.leaveRoomButton.BackColor = System.Drawing.Color.Honeydew;
            this.leaveRoomButton.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.leaveRoomButton.Location = new System.Drawing.Point(326, 354);
            this.leaveRoomButton.Name = "leaveRoomButton";
            this.leaveRoomButton.Size = new System.Drawing.Size(140, 50);
            this.leaveRoomButton.TabIndex = 30;
            this.leaveRoomButton.Text = "Leave Room";
            this.leaveRoomButton.UseVisualStyleBackColor = false;
            this.leaveRoomButton.Click += new System.EventHandler(this.leaveRoomButton_Click);
            // 
            // loggedName
            // 
            this.loggedName.BackColor = System.Drawing.Color.Transparent;
            this.loggedName.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loggedName.ForeColor = System.Drawing.Color.White;
            this.loggedName.Location = new System.Drawing.Point(12, 9);
            this.loggedName.Name = "loggedName";
            this.loggedName.Size = new System.Drawing.Size(145, 32);
            this.loggedName.TabIndex = 31;
            // 
            // waitForGame
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::gui_client_igoad.Properties.Resources.background4;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(776, 518);
            this.Controls.Add(this.loggedName);
            this.Controls.Add(this.leaveRoomButton);
            this.Controls.Add(this.connectedUsers);
            this.Controls.Add(this.currentPartticipantsTitleLabel);
            this.Controls.Add(this.room_name_label);
            this.Controls.Add(this.IT_TRIVIA);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "waitForGame";
            this.Text = "Waiting Room";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.waitForGame_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.waitForGame_FormClosed);
            this.Load += new System.EventHandler(this.waitForGame_Load);
            ((System.ComponentModel.ISupportInitialize)(this.connectedUsers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button IT_TRIVIA;
        private System.Windows.Forms.Label room_name_label;
        private System.Windows.Forms.Label currentPartticipantsTitleLabel;
        private System.Windows.Forms.DataGridView connectedUsers;
        private System.Windows.Forms.Button leaveRoomButton;
        private System.Windows.Forms.Label loggedName;
    }
}