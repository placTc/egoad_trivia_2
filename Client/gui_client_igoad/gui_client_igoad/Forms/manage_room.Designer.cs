﻿namespace gui_client_igoad
{
    partial class manage_room
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(manage_room));
            this.closeRoomButton = new System.Windows.Forms.Button();
            this.connectedUsers = new System.Windows.Forms.DataGridView();
            this.currentPartticipantsTitleLabel = new System.Windows.Forms.Label();
            this.num_of_questionsLabel = new System.Windows.Forms.Label();
            this.time_for_questionsLabel = new System.Windows.Forms.Label();
            this.max_playersLabel = new System.Windows.Forms.Label();
            this.settings2Label = new System.Windows.Forms.Label();
            this.settings3Label = new System.Windows.Forms.Label();
            this.settings1Label = new System.Windows.Forms.Label();
            this.room_name_label = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.StartGameButton = new System.Windows.Forms.Button();
            this.loggedName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.connectedUsers)).BeginInit();
            this.SuspendLayout();
            // 
            // closeRoomButton
            // 
            this.closeRoomButton.BackColor = System.Drawing.Color.Honeydew;
            this.closeRoomButton.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.closeRoomButton.Location = new System.Drawing.Point(326, 354);
            this.closeRoomButton.Name = "closeRoomButton";
            this.closeRoomButton.Size = new System.Drawing.Size(140, 50);
            this.closeRoomButton.TabIndex = 41;
            this.closeRoomButton.Text = "Close Room";
            this.closeRoomButton.UseVisualStyleBackColor = false;
            this.closeRoomButton.Click += new System.EventHandler(this.closeRoomButton_Click);
            // 
            // connectedUsers
            // 
            this.connectedUsers.AllowUserToDeleteRows = false;
            this.connectedUsers.AllowUserToResizeRows = false;
            this.connectedUsers.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.connectedUsers.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.connectedUsers.BackgroundColor = System.Drawing.Color.White;
            this.connectedUsers.ColumnHeadersHeight = 20;
            this.connectedUsers.ColumnHeadersVisible = false;
            this.connectedUsers.EnableHeadersVisualStyles = false;
            this.connectedUsers.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.connectedUsers.Location = new System.Drawing.Point(238, 208);
            this.connectedUsers.Name = "connectedUsers";
            this.connectedUsers.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.connectedUsers.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.connectedUsers.Size = new System.Drawing.Size(308, 123);
            this.connectedUsers.TabIndex = 40;
            // 
            // currentPartticipantsTitleLabel
            // 
            this.currentPartticipantsTitleLabel.BackColor = System.Drawing.Color.Transparent;
            this.currentPartticipantsTitleLabel.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.currentPartticipantsTitleLabel.ForeColor = System.Drawing.Color.White;
            this.currentPartticipantsTitleLabel.Location = new System.Drawing.Point(246, 175);
            this.currentPartticipantsTitleLabel.Name = "currentPartticipantsTitleLabel";
            this.currentPartticipantsTitleLabel.Size = new System.Drawing.Size(300, 30);
            this.currentPartticipantsTitleLabel.TabIndex = 39;
            this.currentPartticipantsTitleLabel.Text = "Current Users Connected:";
            this.currentPartticipantsTitleLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // num_of_questionsLabel
            // 
            this.num_of_questionsLabel.BackColor = System.Drawing.Color.Transparent;
            this.num_of_questionsLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num_of_questionsLabel.ForeColor = System.Drawing.Color.Gray;
            this.num_of_questionsLabel.Location = new System.Drawing.Point(394, 139);
            this.num_of_questionsLabel.Name = "num_of_questionsLabel";
            this.num_of_questionsLabel.Size = new System.Drawing.Size(40, 27);
            this.num_of_questionsLabel.TabIndex = 38;
            this.num_of_questionsLabel.Text = "0";
            // 
            // time_for_questionsLabel
            // 
            this.time_for_questionsLabel.BackColor = System.Drawing.Color.Transparent;
            this.time_for_questionsLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.time_for_questionsLabel.ForeColor = System.Drawing.Color.Gray;
            this.time_for_questionsLabel.Location = new System.Drawing.Point(583, 139);
            this.time_for_questionsLabel.Name = "time_for_questionsLabel";
            this.time_for_questionsLabel.Size = new System.Drawing.Size(46, 27);
            this.time_for_questionsLabel.TabIndex = 37;
            this.time_for_questionsLabel.Text = "0";
            // 
            // max_playersLabel
            // 
            this.max_playersLabel.BackColor = System.Drawing.Color.Transparent;
            this.max_playersLabel.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.max_playersLabel.ForeColor = System.Drawing.Color.Gray;
            this.max_playersLabel.Location = new System.Drawing.Point(272, 139);
            this.max_playersLabel.Name = "max_playersLabel";
            this.max_playersLabel.Size = new System.Drawing.Size(41, 27);
            this.max_playersLabel.TabIndex = 36;
            this.max_playersLabel.Text = "0";
            // 
            // settings2Label
            // 
            this.settings2Label.BackColor = System.Drawing.Color.Transparent;
            this.settings2Label.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settings2Label.ForeColor = System.Drawing.Color.Gray;
            this.settings2Label.Location = new System.Drawing.Point(311, 139);
            this.settings2Label.Name = "settings2Label";
            this.settings2Label.Size = new System.Drawing.Size(87, 27);
            this.settings2Label.TabIndex = 35;
            this.settings2Label.Text = "questions:";
            // 
            // settings3Label
            // 
            this.settings3Label.BackColor = System.Drawing.Color.Transparent;
            this.settings3Label.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settings3Label.ForeColor = System.Drawing.Color.Gray;
            this.settings3Label.Location = new System.Drawing.Point(430, 139);
            this.settings3Label.Name = "settings3Label";
            this.settings3Label.Size = new System.Drawing.Size(155, 27);
            this.settings3Label.TabIndex = 34;
            this.settings3Label.Text = "time_for_question:";
            // 
            // settings1Label
            // 
            this.settings1Label.BackColor = System.Drawing.Color.Transparent;
            this.settings1Label.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.settings1Label.ForeColor = System.Drawing.Color.Gray;
            this.settings1Label.Location = new System.Drawing.Point(165, 139);
            this.settings1Label.Name = "settings1Label";
            this.settings1Label.Size = new System.Drawing.Size(113, 27);
            this.settings1Label.TabIndex = 33;
            this.settings1Label.Text = "max_players:";
            // 
            // room_name_label
            // 
            this.room_name_label.BackColor = System.Drawing.Color.Transparent;
            this.room_name_label.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.room_name_label.ForeColor = System.Drawing.Color.White;
            this.room_name_label.Location = new System.Drawing.Point(246, 109);
            this.room_name_label.Name = "room_name_label";
            this.room_name_label.Size = new System.Drawing.Size(300, 30);
            this.room_name_label.TabIndex = 32;
            this.room_name_label.Text = "Room1";
            this.room_name_label.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button1
            // 
            this.button1.BackgroundImage = global::gui_client_igoad.Properties.Resources.it_logo_new;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Location = new System.Drawing.Point(296, 0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(200, 93);
            this.button1.TabIndex = 31;
            this.button1.UseVisualStyleBackColor = true;
            // 
            // StartGameButton
            // 
            this.StartGameButton.BackColor = System.Drawing.Color.Honeydew;
            this.StartGameButton.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.StartGameButton.Location = new System.Drawing.Point(311, 438);
            this.StartGameButton.Name = "StartGameButton";
            this.StartGameButton.Size = new System.Drawing.Size(170, 57);
            this.StartGameButton.TabIndex = 42;
            this.StartGameButton.Text = "Start Game";
            this.StartGameButton.UseVisualStyleBackColor = false;
            this.StartGameButton.Click += new System.EventHandler(this.StartGameButton_Click);
            // 
            // loggedName
            // 
            this.loggedName.BackColor = System.Drawing.Color.Transparent;
            this.loggedName.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loggedName.ForeColor = System.Drawing.Color.White;
            this.loggedName.Location = new System.Drawing.Point(12, 9);
            this.loggedName.Name = "loggedName";
            this.loggedName.Size = new System.Drawing.Size(145, 32);
            this.loggedName.TabIndex = 43;
            // 
            // manage_room
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::gui_client_igoad.Properties.Resources.background4;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(776, 518);
            this.Controls.Add(this.loggedName);
            this.Controls.Add(this.StartGameButton);
            this.Controls.Add(this.closeRoomButton);
            this.Controls.Add(this.connectedUsers);
            this.Controls.Add(this.currentPartticipantsTitleLabel);
            this.Controls.Add(this.num_of_questionsLabel);
            this.Controls.Add(this.time_for_questionsLabel);
            this.Controls.Add(this.max_playersLabel);
            this.Controls.Add(this.settings2Label);
            this.Controls.Add(this.settings3Label);
            this.Controls.Add(this.settings1Label);
            this.Controls.Add(this.room_name_label);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "manage_room";
            this.Text = "Manage Room";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.manage_room_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.manage_room_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.connectedUsers)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button closeRoomButton;
        private System.Windows.Forms.DataGridView connectedUsers;
        private System.Windows.Forms.Label currentPartticipantsTitleLabel;
        private System.Windows.Forms.Label num_of_questionsLabel;
        private System.Windows.Forms.Label time_for_questionsLabel;
        private System.Windows.Forms.Label max_playersLabel;
        private System.Windows.Forms.Label settings2Label;
        private System.Windows.Forms.Label settings3Label;
        private System.Windows.Forms.Label settings1Label;
        private System.Windows.Forms.Label room_name_label;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button StartGameButton;
        private System.Windows.Forms.Label loggedName;
    }
}