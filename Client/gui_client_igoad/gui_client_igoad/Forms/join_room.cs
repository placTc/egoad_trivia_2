﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace gui_client_igoad
{
    public partial class join_room : Form
    {
        private Button back_button;
        private Button IT_TRIVIA;
        private Label loggedName;
        private Panel panel1;
        BindingList<RoomStructure> listOfRooms = new BindingList<RoomStructure>();
        private DataGridView RoomsDataView;
        private Button join_room_button;
        private readonly object listOfRoomLock = new object();
        Thread thread;
        private Label label1;
        private int selectedId;
        private string selected_room_name;
        private Label label2;

        private delegate void ChangeRoomDataView();
        private ChangeRoomDataView CRDVDelegate;

        private delegate void ClearRoomDataView();
        private ClearRoomDataView ClearRDVDelegate;
        public join_room()
        {
            InitializeComponent();
            RoomsDataView.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            join_room_button.Enabled = false;
            loggedName.Text = Helper.loggedUserName;

            RoomsDataView.DataSource = listOfRooms;
            this.thread = new Thread(new ThreadStart(UpdateRooms));
            thread.Start();
            CRDVDelegate = new ChangeRoomDataView(UpdateRoomView);
            ClearRDVDelegate = new ClearRoomDataView(ClearRoomView);
        }
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(join_room));
            this.IT_TRIVIA = new System.Windows.Forms.Button();
            this.back_button = new System.Windows.Forms.Button();
            this.loggedName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.RoomsDataView = new System.Windows.Forms.DataGridView();
            this.join_room_button = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsDataView)).BeginInit();
            this.SuspendLayout();
            // 
            // IT_TRIVIA
            // 
            this.IT_TRIVIA.BackgroundImage = global::gui_client_igoad.Properties.Resources.it_logo_new;
            this.IT_TRIVIA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IT_TRIVIA.FlatAppearance.BorderSize = 0;
            this.IT_TRIVIA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IT_TRIVIA.Location = new System.Drawing.Point(185, 0);
            this.IT_TRIVIA.Name = "IT_TRIVIA";
            this.IT_TRIVIA.Size = new System.Drawing.Size(264, 137);
            this.IT_TRIVIA.TabIndex = 14;
            this.IT_TRIVIA.UseVisualStyleBackColor = true;
            // 
            // back_button
            // 
            this.back_button.BackColor = System.Drawing.Color.Honeydew;
            this.back_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.back_button.Location = new System.Drawing.Point(536, 572);
            this.back_button.Name = "back_button";
            this.back_button.Size = new System.Drawing.Size(76, 40);
            this.back_button.TabIndex = 15;
            this.back_button.Text = "Back";
            this.back_button.UseVisualStyleBackColor = false;
            this.back_button.Click += new System.EventHandler(this.back_button_Click_1);
            // 
            // loggedName
            // 
            this.loggedName.BackColor = System.Drawing.Color.Transparent;
            this.loggedName.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loggedName.ForeColor = System.Drawing.Color.White;
            this.loggedName.Location = new System.Drawing.Point(12, 9);
            this.loggedName.Name = "loggedName";
            this.loggedName.Size = new System.Drawing.Size(145, 32);
            this.loggedName.TabIndex = 16;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.RoomsDataView);
            this.panel1.Location = new System.Drawing.Point(116, 172);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(399, 302);
            this.panel1.TabIndex = 18;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.label1.Location = new System.Drawing.Point(149, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 20;
            this.label1.Text = "Rooms:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // RoomsDataView
            // 
            this.RoomsDataView.AllowUserToDeleteRows = false;
            this.RoomsDataView.AllowUserToResizeRows = false;
            this.RoomsDataView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.RoomsDataView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCellsExceptHeaders;
            this.RoomsDataView.BackgroundColor = System.Drawing.Color.White;
            this.RoomsDataView.ColumnHeadersHeight = 20;
            this.RoomsDataView.ColumnHeadersVisible = false;
            this.RoomsDataView.EnableHeadersVisualStyles = false;
            this.RoomsDataView.GridColor = System.Drawing.SystemColors.ActiveBorder;
            this.RoomsDataView.Location = new System.Drawing.Point(8, 57);
            this.RoomsDataView.Name = "RoomsDataView";
            this.RoomsDataView.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.RoomsDataView.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.RoomsDataView.Size = new System.Drawing.Size(383, 188);
            this.RoomsDataView.TabIndex = 16;
            this.RoomsDataView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.RoomsDataView_CellContentClick);
            // 
            // join_room_button
            // 
            this.join_room_button.BackColor = System.Drawing.Color.White;
            this.join_room_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.join_room_button.ForeColor = System.Drawing.Color.Black;
            this.join_room_button.Location = new System.Drawing.Point(257, 480);
            this.join_room_button.Name = "join_room_button";
            this.join_room_button.Size = new System.Drawing.Size(123, 39);
            this.join_room_button.TabIndex = 19;
            this.join_room_button.Text = "Join Room";
            this.join_room_button.UseVisualStyleBackColor = false;
            this.join_room_button.Click += new System.EventHandler(this.join_room_button_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(151, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 21;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // join_room
            // 
            this.BackgroundImage = global::gui_client_igoad.Properties.Resources.background3;
            this.ClientSize = new System.Drawing.Size(624, 624);
            this.Controls.Add(this.join_room_button);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.loggedName);
            this.Controls.Add(this.back_button);
            this.Controls.Add(this.IT_TRIVIA);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "join_room";
            this.Text = "Join Room";
            this.Load += new System.EventHandler(this.join_room_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsDataView)).EndInit();
            this.ResumeLayout(false);

        }


        public class RoomStructure
        {
            public int RoomId { get; set; }
            public string RoomName { get; set; }
            public string players { get; set; }
        }
        private bool JoinRoom(int roomId)
        {
            try
            {
                List<byte> request = SerializationFunctions.SerializeJoinRoomRequest(roomId);
                Networking.SendData(request);
                string response = Networking.ReceiveData();

                if (!SerializationFunctions.ParseGenericResponse(response))
                {
                    label2.Text = SerializationFunctions.ParseErrorResponse(response);
                    return false;
                }
                label2.Text = "";
                return true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
                return false;
            }
        }

        private void UpdateRooms()
        {
            while(true)
            {
                GetRoomsFromServer();
                lock (listOfRoomLock)
                {
                    Thread.Sleep(2500);
                }
            }
        }

        private void UpdateRoomView()
        {
            listOfRooms.Add(rs);
        }

        private void ClearRoomView()
        {
            listOfRooms.Clear();
        }

        private void back_button_Click(object sender, EventArgs e)
        {
            try { thread.Abort(); } 
            catch { }
            this.Close();
        }

        private void back_button_Click_1(object sender, EventArgs e)
        {
            try { thread.Abort(); }
            catch { }
            this.Close();
        }

        RoomStructure rs;

        private void GetRoomsFromServer()
        {
            try
            {
                List<byte> bytes = new List<byte>();
                bytes.Add(Convert.ToByte(Helper.GETROOMS_CODE));
                bytes.AddRange(Encoding.ASCII.GetBytes(Helper.ZERO_LENGTH));
                Networking.SendData(bytes);
                string response = Networking.ReceiveData();
                Tuple<List<string>, List<int>> tuple = SerializationFunctions.ParseGetRooms(response);
                lock (listOfRoomLock)
                {
                    this.Invoke(ClearRDVDelegate);
                    for (int i = 0; i < tuple.Item1.Count; i++)
                    {
                        string players = string.Empty;
                        foreach (string element in Helper.GetUsersInRoom(tuple.Item2[i]))
                        {
                            players += element;
                            players += ", ";
                        }
                        players = players.Substring(0, players.Length - 2);
                        rs = new RoomStructure() { RoomId = tuple.Item2[i], RoomName = tuple.Item1[i], players = players };
                        this.Invoke(CRDVDelegate);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
        }

        private void join_room_Load(object sender, EventArgs e)
        {
            
        }

        private void RoomsDataView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            selectedId = (RoomsDataView.SelectedRows[0].DataBoundItem as RoomStructure).RoomId;
            selected_room_name = (RoomsDataView.SelectedRows[0].DataBoundItem as RoomStructure).RoomName;
            join_room_button.Enabled = true;
        }


        private void join_room_button_Click(object sender, EventArgs e)
        {
            Helper.roomID = selectedId;
            Helper.room_joined = selected_room_name;

            if(JoinRoom(selectedId))
            {
                Helper.room_joined = selected_room_name;
                waitForGame frm = new waitForGame();
                frm.Location = this.Location;
                frm.StartPosition = FormStartPosition.Manual;
                frm.FormClosing += delegate { this.Show(); };
                frm.Show();
                this.Hide();
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
