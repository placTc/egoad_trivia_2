﻿namespace gui_client_igoad
{
    partial class create_room
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.back_button = new System.Windows.Forms.Button();
            this.IT_TRIVIA = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.time_for_questions_textBox = new System.Windows.Forms.TextBox();
            this.num_of_questions_textBox = new System.Windows.Forms.TextBox();
            this.num_of_players_textBox = new System.Windows.Forms.TextBox();
            this.room_name_textBox = new System.Windows.Forms.TextBox();
            this.create_room_button = new System.Windows.Forms.Button();
            this.time_for_question_label = new System.Windows.Forms.Label();
            this.num_of_questions_label = new System.Windows.Forms.Label();
            this.number_of_players_label = new System.Windows.Forms.Label();
            this.room_name_label = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // back_button
            // 
            this.back_button.BackColor = System.Drawing.Color.Honeydew;
            this.back_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.back_button.Location = new System.Drawing.Point(536, 572);
            this.back_button.Name = "back_button";
            this.back_button.Size = new System.Drawing.Size(76, 40);
            this.back_button.TabIndex = 14;
            this.back_button.Text = "Back";
            this.back_button.UseVisualStyleBackColor = false;
            this.back_button.Click += new System.EventHandler(this.back_button_Click);
            // 
            // IT_TRIVIA
            // 
            this.IT_TRIVIA.BackgroundImage = global::gui_client_igoad.Properties.Resources.it_logo_new;
            this.IT_TRIVIA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IT_TRIVIA.FlatAppearance.BorderSize = 0;
            this.IT_TRIVIA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IT_TRIVIA.Location = new System.Drawing.Point(185, 0);
            this.IT_TRIVIA.Name = "IT_TRIVIA";
            this.IT_TRIVIA.Size = new System.Drawing.Size(264, 137);
            this.IT_TRIVIA.TabIndex = 13;
            this.IT_TRIVIA.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.time_for_questions_textBox);
            this.panel1.Controls.Add(this.num_of_questions_textBox);
            this.panel1.Controls.Add(this.num_of_players_textBox);
            this.panel1.Controls.Add(this.room_name_textBox);
            this.panel1.Controls.Add(this.create_room_button);
            this.panel1.Controls.Add(this.time_for_question_label);
            this.panel1.Controls.Add(this.num_of_questions_label);
            this.panel1.Controls.Add(this.number_of_players_label);
            this.panel1.Controls.Add(this.room_name_label);
            this.panel1.Location = new System.Drawing.Point(81, 192);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(463, 240);
            this.panel1.TabIndex = 17;
            // 
            // time_for_questions_textBox
            // 
            this.time_for_questions_textBox.Location = new System.Drawing.Point(209, 138);
            this.time_for_questions_textBox.Name = "time_for_questions_textBox";
            this.time_for_questions_textBox.Size = new System.Drawing.Size(64, 20);
            this.time_for_questions_textBox.TabIndex = 8;
            // 
            // num_of_questions_textBox
            // 
            this.num_of_questions_textBox.Location = new System.Drawing.Point(225, 100);
            this.num_of_questions_textBox.Name = "num_of_questions_textBox";
            this.num_of_questions_textBox.Size = new System.Drawing.Size(209, 20);
            this.num_of_questions_textBox.TabIndex = 7;
            // 
            // num_of_players_textBox
            // 
            this.num_of_players_textBox.Location = new System.Drawing.Point(198, 61);
            this.num_of_players_textBox.Name = "num_of_players_textBox";
            this.num_of_players_textBox.Size = new System.Drawing.Size(170, 20);
            this.num_of_players_textBox.TabIndex = 6;
            // 
            // room_name_textBox
            // 
            this.room_name_textBox.Location = new System.Drawing.Point(133, 26);
            this.room_name_textBox.Name = "room_name_textBox";
            this.room_name_textBox.Size = new System.Drawing.Size(165, 20);
            this.room_name_textBox.TabIndex = 5;
            // 
            // create_room_button
            // 
            this.create_room_button.BackColor = System.Drawing.Color.Honeydew;
            this.create_room_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.create_room_button.Location = new System.Drawing.Point(158, 191);
            this.create_room_button.Name = "create_room_button";
            this.create_room_button.Size = new System.Drawing.Size(151, 46);
            this.create_room_button.TabIndex = 4;
            this.create_room_button.Text = "Create Room";
            this.create_room_button.UseVisualStyleBackColor = false;
            // 
            // time_for_question_label
            // 
            this.time_for_question_label.AutoSize = true;
            this.time_for_question_label.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.time_for_question_label.Location = new System.Drawing.Point(8, 131);
            this.time_for_question_label.Name = "time_for_question_label";
            this.time_for_question_label.Size = new System.Drawing.Size(209, 27);
            this.time_for_question_label.TabIndex = 3;
            this.time_for_question_label.Text = "Time For Questions: ";
            // 
            // num_of_questions_label
            // 
            this.num_of_questions_label.AutoSize = true;
            this.num_of_questions_label.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.num_of_questions_label.Location = new System.Drawing.Point(8, 93);
            this.num_of_questions_label.Name = "num_of_questions_label";
            this.num_of_questions_label.Size = new System.Drawing.Size(225, 27);
            this.num_of_questions_label.TabIndex = 2;
            this.num_of_questions_label.Text = "Number of Questions: ";
            // 
            // number_of_players_label
            // 
            this.number_of_players_label.AutoSize = true;
            this.number_of_players_label.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.number_of_players_label.Location = new System.Drawing.Point(8, 54);
            this.number_of_players_label.Name = "number_of_players_label";
            this.number_of_players_label.Size = new System.Drawing.Size(201, 27);
            this.number_of_players_label.TabIndex = 1;
            this.number_of_players_label.Text = "Number of Players: ";
            // 
            // room_name_label
            // 
            this.room_name_label.AutoSize = true;
            this.room_name_label.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.room_name_label.Location = new System.Drawing.Point(8, 19);
            this.room_name_label.Name = "room_name_label";
            this.room_name_label.Size = new System.Drawing.Size(135, 27);
            this.room_name_label.TabIndex = 0;
            this.room_name_label.Text = "Room Name: ";
            // 
            // create_room
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::gui_client_igoad.Properties.Resources.background3;
            this.ClientSize = new System.Drawing.Size(624, 624);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.back_button);
            this.Controls.Add(this.IT_TRIVIA);
            this.Name = "create_room";
            this.Text = "create_room";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button IT_TRIVIA;
        private System.Windows.Forms.Button back_button;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label time_for_question_label;
        private System.Windows.Forms.Label num_of_questions_label;
        private System.Windows.Forms.Label number_of_players_label;
        private System.Windows.Forms.Label room_name_label;
        private System.Windows.Forms.TextBox time_for_questions_textBox;
        private System.Windows.Forms.TextBox num_of_questions_textBox;
        private System.Windows.Forms.TextBox num_of_players_textBox;
        private System.Windows.Forms.TextBox room_name_textBox;
        private System.Windows.Forms.Button create_room_button;
    }
}