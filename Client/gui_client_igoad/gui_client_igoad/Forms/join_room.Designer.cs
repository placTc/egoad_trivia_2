﻿namespace gui_client_igoad
{
    partial class join_room
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IT_TRIVIA = new System.Windows.Forms.Button();
            this.back_button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // IT_TRIVIA
            // 
            this.IT_TRIVIA.BackgroundImage = global::gui_client_igoad.Properties.Resources.it_logo_new;
            this.IT_TRIVIA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IT_TRIVIA.FlatAppearance.BorderSize = 0;
            this.IT_TRIVIA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IT_TRIVIA.Location = new System.Drawing.Point(185, 0);
            this.IT_TRIVIA.Name = "IT_TRIVIA";
            this.IT_TRIVIA.Size = new System.Drawing.Size(264, 137);
            this.IT_TRIVIA.TabIndex = 13;
            this.IT_TRIVIA.UseVisualStyleBackColor = true;
            // 
            // back_button
            // 
            this.back_button.BackColor = System.Drawing.Color.Honeydew;
            this.back_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.back_button.Location = new System.Drawing.Point(536, 572);
            this.back_button.Name = "back_button";
            this.back_button.Size = new System.Drawing.Size(76, 40);
            this.back_button.TabIndex = 14;
            this.back_button.Text = "Back";
            this.back_button.UseVisualStyleBackColor = false;
            this.back_button.Click += new System.EventHandler(this.back_button_Click);
            // 
            // join_room
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::gui_client_igoad.Properties.Resources.background3;
            this.ClientSize = new System.Drawing.Size(624, 624);
            this.Controls.Add(this.back_button);
            this.Controls.Add(this.IT_TRIVIA);
            this.Name = "join_room";
            this.Text = "join_room";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button IT_TRIVIA;
        private System.Windows.Forms.Button back_button;
    }
}