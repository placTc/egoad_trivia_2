﻿namespace gui_client_igoad
{
    partial class mainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainMenu));
            this.sign_in_panel = new System.Windows.Forms.Panel();
            this.eye_butt = new System.Windows.Forms.Button();
            this.invalid_message = new System.Windows.Forms.Label();
            this.sign_in_butt = new System.Windows.Forms.Button();
            this.textBox_password = new System.Windows.Forms.TextBox();
            this.textBox_username = new System.Windows.Forms.TextBox();
            this.troll_butt = new System.Windows.Forms.Button();
            this.password_label = new System.Windows.Forms.Label();
            this.username_label = new System.Windows.Forms.Label();
            this.Sign_up_button = new System.Windows.Forms.Button();
            this.join_room_butt = new System.Windows.Forms.Button();
            this.create_room_butt = new System.Windows.Forms.Button();
            this.Welcome_lable = new System.Windows.Forms.Label();
            this.IT_TRIVIA = new System.Windows.Forms.Button();
            this.loggedName = new System.Windows.Forms.Label();
            this.sign_in_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sign_in_panel
            // 
            this.sign_in_panel.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.sign_in_panel.Controls.Add(this.eye_butt);
            this.sign_in_panel.Controls.Add(this.invalid_message);
            this.sign_in_panel.Controls.Add(this.sign_in_butt);
            this.sign_in_panel.Controls.Add(this.textBox_password);
            this.sign_in_panel.Controls.Add(this.textBox_username);
            this.sign_in_panel.Controls.Add(this.troll_butt);
            this.sign_in_panel.Controls.Add(this.password_label);
            this.sign_in_panel.Controls.Add(this.username_label);
            this.sign_in_panel.Location = new System.Drawing.Point(162, 165);
            this.sign_in_panel.Name = "sign_in_panel";
            this.sign_in_panel.Size = new System.Drawing.Size(316, 158);
            this.sign_in_panel.TabIndex = 1;
            // 
            // eye_butt
            // 
            this.eye_butt.BackgroundImage = global::gui_client_igoad.Properties.Resources.not_eye_butt;
            this.eye_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.eye_butt.FlatAppearance.BorderColor = System.Drawing.SystemColors.ActiveCaption;
            this.eye_butt.FlatAppearance.BorderSize = 0;
            this.eye_butt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eye_butt.Location = new System.Drawing.Point(274, 43);
            this.eye_butt.Name = "eye_butt";
            this.eye_butt.Size = new System.Drawing.Size(37, 27);
            this.eye_butt.TabIndex = 12;
            this.eye_butt.UseVisualStyleBackColor = true;
            this.eye_butt.Click += new System.EventHandler(this.eye_butt_Click);
            // 
            // invalid_message
            // 
            this.invalid_message.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.invalid_message.Location = new System.Drawing.Point(90, 71);
            this.invalid_message.Name = "invalid_message";
            this.invalid_message.Size = new System.Drawing.Size(154, 23);
            this.invalid_message.TabIndex = 1;
            this.invalid_message.Text = "invalid username or password!";
            // 
            // sign_in_butt
            // 
            this.sign_in_butt.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.sign_in_butt.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.sign_in_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.sign_in_butt.FlatAppearance.BorderSize = 0;
            this.sign_in_butt.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.sign_in_butt.Location = new System.Drawing.Point(93, 97);
            this.sign_in_butt.Name = "sign_in_butt";
            this.sign_in_butt.Size = new System.Drawing.Size(133, 52);
            this.sign_in_butt.TabIndex = 4;
            this.sign_in_butt.Text = "Sign In";
            this.sign_in_butt.UseVisualStyleBackColor = false;
            this.sign_in_butt.Click += new System.EventHandler(this.sign_in_butt_Click);
            // 
            // textBox_password
            // 
            this.textBox_password.Location = new System.Drawing.Point(81, 48);
            this.textBox_password.Name = "textBox_password";
            this.textBox_password.Size = new System.Drawing.Size(187, 20);
            this.textBox_password.TabIndex = 3;
            this.textBox_password.UseSystemPasswordChar = true;
            this.textBox_password.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_password_KeyPress);
            // 
            // textBox_username
            // 
            this.textBox_username.Location = new System.Drawing.Point(81, 22);
            this.textBox_username.Name = "textBox_username";
            this.textBox_username.Size = new System.Drawing.Size(187, 20);
            this.textBox_username.TabIndex = 2;
            this.textBox_username.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox_username_KeyDown);
            // 
            // troll_butt
            // 
            this.troll_butt.BackgroundImage = global::gui_client_igoad.Properties.Resources.transparent_troll;
            this.troll_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.troll_butt.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.troll_butt.FlatAppearance.BorderSize = 0;
            this.troll_butt.FlatAppearance.MouseDownBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.troll_butt.FlatAppearance.MouseOverBackColor = System.Drawing.SystemColors.ActiveCaption;
            this.troll_butt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.troll_butt.Location = new System.Drawing.Point(265, 85);
            this.troll_butt.Name = "troll_butt";
            this.troll_butt.Size = new System.Drawing.Size(48, 42);
            this.troll_butt.TabIndex = 6;
            this.troll_butt.UseVisualStyleBackColor = true;
            this.troll_butt.MouseHover += new System.EventHandler(this.troll_butt_MouseHover);
            // 
            // password_label
            // 
            this.password_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.password_label.Location = new System.Drawing.Point(3, 48);
            this.password_label.Name = "password_label";
            this.password_label.Size = new System.Drawing.Size(81, 26);
            this.password_label.TabIndex = 1;
            this.password_label.Text = "Password:";
            // 
            // username_label
            // 
            this.username_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.username_label.Location = new System.Drawing.Point(3, 22);
            this.username_label.Name = "username_label";
            this.username_label.Size = new System.Drawing.Size(81, 18);
            this.username_label.TabIndex = 0;
            this.username_label.Text = "Username:";
            // 
            // Sign_up_button
            // 
            this.Sign_up_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Sign_up_button.Location = new System.Drawing.Point(255, 351);
            this.Sign_up_button.Name = "Sign_up_button";
            this.Sign_up_button.Size = new System.Drawing.Size(133, 54);
            this.Sign_up_button.TabIndex = 7;
            this.Sign_up_button.Text = "Sign Up";
            this.Sign_up_button.UseVisualStyleBackColor = true;
            this.Sign_up_button.Click += new System.EventHandler(this.Sign_up_button_Click);
            // 
            // join_room_butt
            // 
            this.join_room_butt.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.join_room_butt.Location = new System.Drawing.Point(255, 411);
            this.join_room_butt.Name = "join_room_butt";
            this.join_room_butt.Size = new System.Drawing.Size(133, 54);
            this.join_room_butt.TabIndex = 8;
            this.join_room_butt.Text = "Join Room";
            this.join_room_butt.UseVisualStyleBackColor = true;
            this.join_room_butt.Click += new System.EventHandler(this.join_room_butt_Click);
            // 
            // create_room_butt
            // 
            this.create_room_butt.Font = new System.Drawing.Font("Comic Sans MS", 12.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.create_room_butt.Location = new System.Drawing.Point(255, 471);
            this.create_room_butt.Name = "create_room_butt";
            this.create_room_butt.Size = new System.Drawing.Size(133, 54);
            this.create_room_butt.TabIndex = 9;
            this.create_room_butt.Text = "Create Room";
            this.create_room_butt.UseVisualStyleBackColor = true;
            this.create_room_butt.Click += new System.EventHandler(this.create_room_butt_Click);
            // 
            // Welcome_lable
            // 
            this.Welcome_lable.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.Welcome_lable.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.Welcome_lable.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Welcome_lable.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.Welcome_lable.Location = new System.Drawing.Point(177, 295);
            this.Welcome_lable.Name = "Welcome_lable";
            this.Welcome_lable.Size = new System.Drawing.Size(273, 43);
            this.Welcome_lable.TabIndex = 5;
            this.Welcome_lable.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Welcome_lable.Visible = false;
            // 
            // IT_TRIVIA
            // 
            this.IT_TRIVIA.BackgroundImage = global::gui_client_igoad.Properties.Resources.it_logo_new;
            this.IT_TRIVIA.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.IT_TRIVIA.FlatAppearance.BorderSize = 0;
            this.IT_TRIVIA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.IT_TRIVIA.Location = new System.Drawing.Point(185, -1);
            this.IT_TRIVIA.Name = "IT_TRIVIA";
            this.IT_TRIVIA.Size = new System.Drawing.Size(264, 137);
            this.IT_TRIVIA.TabIndex = 12;
            this.IT_TRIVIA.UseVisualStyleBackColor = true;
            // 
            // loggedName
            // 
            this.loggedName.BackColor = System.Drawing.Color.Transparent;
            this.loggedName.Font = new System.Drawing.Font("Comic Sans MS", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loggedName.Location = new System.Drawing.Point(12, 9);
            this.loggedName.Name = "loggedName";
            this.loggedName.Size = new System.Drawing.Size(145, 32);
            this.loggedName.TabIndex = 13;
            // 
            // mainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(624, 624);
            this.Controls.Add(this.loggedName);
            this.Controls.Add(this.IT_TRIVIA);
            this.Controls.Add(this.Welcome_lable);
            this.Controls.Add(this.create_room_butt);
            this.Controls.Add(this.join_room_butt);
            this.Controls.Add(this.Sign_up_button);
            this.Controls.Add(this.sign_in_panel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mainMenu";
            this.Text = "Main Menu";
            this.sign_in_panel.ResumeLayout(false);
            this.sign_in_panel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel sign_in_panel;
        private System.Windows.Forms.Label invalid_message;
        private System.Windows.Forms.Button sign_in_butt;
        private System.Windows.Forms.TextBox textBox_password;
        private System.Windows.Forms.TextBox textBox_username;
        private System.Windows.Forms.Label password_label;
        private System.Windows.Forms.Label username_label;
        private System.Windows.Forms.Button troll_butt;
        private System.Windows.Forms.Button Sign_up_button;
        private System.Windows.Forms.Button join_room_butt;
        private System.Windows.Forms.Button create_room_butt;
        private System.Windows.Forms.Label Welcome_lable;
        private System.Windows.Forms.Button eye_butt;
        private System.Windows.Forms.Button IT_TRIVIA;
        private System.Windows.Forms.Label loggedName;
    }
}

