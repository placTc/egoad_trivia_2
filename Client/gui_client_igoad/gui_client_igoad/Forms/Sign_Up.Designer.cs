﻿namespace gui_client_igoad
{
    partial class Sign_Up
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Sign_Up));
            this.sign_up_panel = new System.Windows.Forms.Panel();
            this.birthDateTextBox = new System.Windows.Forms.TextBox();
            this.birthDateLabel = new System.Windows.Forms.Label();
            this.troll_butt = new System.Windows.Forms.Button();
            this.sign_up_button = new System.Windows.Forms.Button();
            this.invalid_message_label = new System.Windows.Forms.Label();
            this.adressTextBox = new System.Windows.Forms.TextBox();
            this.phoneNumTextBox = new System.Windows.Forms.TextBox();
            this.usernameTextBox = new System.Windows.Forms.TextBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.emailTextBox = new System.Windows.Forms.TextBox();
            this.phoneNumberLabel = new System.Windows.Forms.Label();
            this.adressLabel = new System.Windows.Forms.Label();
            this.emailLabel = new System.Windows.Forms.Label();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.userNameLabel = new System.Windows.Forms.Label();
            this.back_button = new System.Windows.Forms.Button();
            this.info_panel = new System.Windows.Forms.Panel();
            this.info1_label = new System.Windows.Forms.Label();
            this.info2_label = new System.Windows.Forms.Label();
            this.info3_label = new System.Windows.Forms.Label();
            this.sign_up_panel.SuspendLayout();
            this.info_panel.SuspendLayout();
            this.SuspendLayout();
            // 
            // sign_up_panel
            // 
            this.sign_up_panel.BackColor = System.Drawing.Color.Azure;
            this.sign_up_panel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sign_up_panel.Controls.Add(this.birthDateTextBox);
            this.sign_up_panel.Controls.Add(this.birthDateLabel);
            this.sign_up_panel.Controls.Add(this.troll_butt);
            this.sign_up_panel.Controls.Add(this.sign_up_button);
            this.sign_up_panel.Controls.Add(this.invalid_message_label);
            this.sign_up_panel.Controls.Add(this.adressTextBox);
            this.sign_up_panel.Controls.Add(this.phoneNumTextBox);
            this.sign_up_panel.Controls.Add(this.usernameTextBox);
            this.sign_up_panel.Controls.Add(this.passwordTextBox);
            this.sign_up_panel.Controls.Add(this.emailTextBox);
            this.sign_up_panel.Controls.Add(this.phoneNumberLabel);
            this.sign_up_panel.Controls.Add(this.adressLabel);
            this.sign_up_panel.Controls.Add(this.emailLabel);
            this.sign_up_panel.Controls.Add(this.passwordLabel);
            this.sign_up_panel.Controls.Add(this.userNameLabel);
            this.sign_up_panel.Location = new System.Drawing.Point(124, 212);
            this.sign_up_panel.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sign_up_panel.Name = "sign_up_panel";
            this.sign_up_panel.Size = new System.Drawing.Size(570, 321);
            this.sign_up_panel.TabIndex = 1;
            // 
            // birthDateTextBox
            // 
            this.birthDateTextBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.birthDateTextBox.Location = new System.Drawing.Point(84, 188);
            this.birthDateTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.birthDateTextBox.Name = "birthDateTextBox";
            this.birthDateTextBox.Size = new System.Drawing.Size(247, 22);
            this.birthDateTextBox.TabIndex = 14;
            this.birthDateTextBox.Text = "DD/MM/YYYY";
            this.birthDateTextBox.Enter += new System.EventHandler(this.birthDateTextBox_Enter);
            this.birthDateTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.birthDateTextBox_KeyPress);
            // 
            // birthDateLabel
            // 
            this.birthDateLabel.Location = new System.Drawing.Point(11, 188);
            this.birthDateLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.birthDateLabel.Name = "birthDateLabel";
            this.birthDateLabel.Size = new System.Drawing.Size(133, 28);
            this.birthDateLabel.TabIndex = 13;
            this.birthDateLabel.Text = "birthDate:";
            // 
            // troll_butt
            // 
            this.troll_butt.BackgroundImage = global::gui_client_igoad.Properties.Resources.transparent_troll;
            this.troll_butt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.troll_butt.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.troll_butt.FlatAppearance.BorderSize = 0;
            this.troll_butt.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Azure;
            this.troll_butt.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Azure;
            this.troll_butt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.troll_butt.Location = new System.Drawing.Point(15, 257);
            this.troll_butt.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.troll_butt.Name = "troll_butt";
            this.troll_butt.Size = new System.Drawing.Size(64, 52);
            this.troll_butt.TabIndex = 7;
            this.troll_butt.UseVisualStyleBackColor = true;
            this.troll_butt.MouseHover += new System.EventHandler(this.troll_butt_MouseHover);
            // 
            // sign_up_button
            // 
            this.sign_up_button.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.sign_up_button.BackColor = System.Drawing.Color.Honeydew;
            this.sign_up_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.sign_up_button.Location = new System.Drawing.Point(193, 257);
            this.sign_up_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.sign_up_button.Name = "sign_up_button";
            this.sign_up_button.Size = new System.Drawing.Size(231, 58);
            this.sign_up_button.TabIndex = 12;
            this.sign_up_button.Text = "Sign Up";
            this.sign_up_button.UseVisualStyleBackColor = false;
            this.sign_up_button.Click += new System.EventHandler(this.sign_up_button_Click);
            // 
            // invalid_message_label
            // 
            this.invalid_message_label.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.invalid_message_label.BackColor = System.Drawing.Color.Azure;
            this.invalid_message_label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.invalid_message_label.ForeColor = System.Drawing.Color.Azure;
            this.invalid_message_label.Location = new System.Drawing.Point(135, 226);
            this.invalid_message_label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.invalid_message_label.Name = "invalid_message_label";
            this.invalid_message_label.Size = new System.Drawing.Size(328, 27);
            this.invalid_message_label.TabIndex = 11;
            this.invalid_message_label.Text = "invalid parameters for sign up";
            // 
            // adressTextBox
            // 
            this.adressTextBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.adressTextBox.Location = new System.Drawing.Point(71, 124);
            this.adressTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.adressTextBox.Name = "adressTextBox";
            this.adressTextBox.Size = new System.Drawing.Size(291, 22);
            this.adressTextBox.TabIndex = 5;
            this.adressTextBox.Text = "(Street, Apt, City)";
            this.adressTextBox.Enter += new System.EventHandler(this.adressTextBox_Enter);
            this.adressTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.adressTextBox_KeyDown);
            // 
            // phoneNumTextBox
            // 
            this.phoneNumTextBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.phoneNumTextBox.Location = new System.Drawing.Point(115, 156);
            this.phoneNumTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.phoneNumTextBox.Name = "phoneNumTextBox";
            this.phoneNumTextBox.Size = new System.Drawing.Size(247, 22);
            this.phoneNumTextBox.TabIndex = 4;
            this.phoneNumTextBox.Text = "01-2345678";
            this.phoneNumTextBox.Enter += new System.EventHandler(this.phoneNumTextBox_Enter);
            this.phoneNumTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.phoneNumTextBox_KeyDown);
            this.phoneNumTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.phoneNumTextBox_KeyPress);
            // 
            // usernameTextBox
            // 
            this.usernameTextBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.usernameTextBox.Location = new System.Drawing.Point(84, 28);
            this.usernameTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.usernameTextBox.Name = "usernameTextBox";
            this.usernameTextBox.Size = new System.Drawing.Size(137, 22);
            this.usernameTextBox.TabIndex = 1;
            this.usernameTextBox.Text = "username";
            this.usernameTextBox.Enter += new System.EventHandler(this.usernameTextBox_Enter);
            this.usernameTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.usernameTextBox_KeyDown);
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.passwordTextBox.Location = new System.Drawing.Point(84, 60);
            this.passwordTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.Size = new System.Drawing.Size(460, 22);
            this.passwordTextBox.TabIndex = 2;
            this.passwordTextBox.Text = "password (at least 8 chars, upper and lower case, special chars , nums)";
            this.passwordTextBox.Enter += new System.EventHandler(this.passwordTextBox_Enter);
            this.passwordTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.passwordTextBox_KeyDown);
            // 
            // emailTextBox
            // 
            this.emailTextBox.ForeColor = System.Drawing.SystemColors.InactiveCaption;
            this.emailTextBox.Location = new System.Drawing.Point(57, 91);
            this.emailTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.emailTextBox.Name = "emailTextBox";
            this.emailTextBox.Size = new System.Drawing.Size(261, 22);
            this.emailTextBox.TabIndex = 3;
            this.emailTextBox.Text = "someone@example.something";
            this.emailTextBox.Enter += new System.EventHandler(this.emailTextBox_Enter);
            this.emailTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.emailTextBox_KeyDown);
            // 
            // phoneNumberLabel
            // 
            this.phoneNumberLabel.Location = new System.Drawing.Point(11, 159);
            this.phoneNumberLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.phoneNumberLabel.Name = "phoneNumberLabel";
            this.phoneNumberLabel.Size = new System.Drawing.Size(133, 28);
            this.phoneNumberLabel.TabIndex = 10;
            this.phoneNumberLabel.Text = "phoneNumber:";
            // 
            // adressLabel
            // 
            this.adressLabel.Location = new System.Drawing.Point(11, 130);
            this.adressLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.adressLabel.Name = "adressLabel";
            this.adressLabel.Size = new System.Drawing.Size(133, 28);
            this.adressLabel.TabIndex = 9;
            this.adressLabel.Text = "adress:";
            // 
            // emailLabel
            // 
            this.emailLabel.Location = new System.Drawing.Point(11, 95);
            this.emailLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.emailLabel.Name = "emailLabel";
            this.emailLabel.Size = new System.Drawing.Size(133, 28);
            this.emailLabel.TabIndex = 8;
            this.emailLabel.Text = "email:";
            // 
            // passwordLabel
            // 
            this.passwordLabel.Location = new System.Drawing.Point(11, 63);
            this.passwordLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(133, 28);
            this.passwordLabel.TabIndex = 7;
            this.passwordLabel.Text = "password:";
            // 
            // userNameLabel
            // 
            this.userNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.userNameLabel.Location = new System.Drawing.Point(11, 31);
            this.userNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.userNameLabel.Name = "userNameLabel";
            this.userNameLabel.Size = new System.Drawing.Size(133, 28);
            this.userNameLabel.TabIndex = 6;
            this.userNameLabel.Text = "username:";
            // 
            // back_button
            // 
            this.back_button.BackColor = System.Drawing.Color.Honeydew;
            this.back_button.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))));
            this.back_button.Location = new System.Drawing.Point(715, 704);
            this.back_button.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.back_button.Name = "back_button";
            this.back_button.Size = new System.Drawing.Size(101, 49);
            this.back_button.TabIndex = 15;
            this.back_button.Text = "Back";
            this.back_button.UseVisualStyleBackColor = false;
            this.back_button.Click += new System.EventHandler(this.back_button_Click);
            // 
            // info_panel
            // 
            this.info_panel.BackColor = System.Drawing.Color.Red;
            this.info_panel.Controls.Add(this.info3_label);
            this.info_panel.Controls.Add(this.info2_label);
            this.info_panel.Controls.Add(this.info1_label);
            this.info_panel.Location = new System.Drawing.Point(240, 540);
            this.info_panel.Name = "info_panel";
            this.info_panel.Size = new System.Drawing.Size(394, 100);
            this.info_panel.TabIndex = 15;
            // 
            // info1_label
            // 
            this.info1_label.Location = new System.Drawing.Point(0, 0);
            this.info1_label.Name = "info1_label";
            this.info1_label.Size = new System.Drawing.Size(391, 25);
            this.info1_label.TabIndex = 0;
            this.info1_label.Text = "check that your adress is in this structure: (Street,Apt number,City)";
            // 
            // info2_label
            // 
            this.info2_label.Location = new System.Drawing.Point(0, 25);
            this.info2_label.Name = "info2_label";
            this.info2_label.Size = new System.Drawing.Size(394, 43);
            this.info2_label.TabIndex = 1;
            this.info2_label.Text = "check that you password has at least 8 chars, upper and lower case, special chars" +
    " and number";
            // 
            // info3_label
            // 
            this.info3_label.Location = new System.Drawing.Point(-3, 68);
            this.info3_label.Name = "info3_label";
            this.info3_label.Size = new System.Drawing.Size(394, 19);
            this.info3_label.TabIndex = 2;
            this.info3_label.Text = "check that your birth date is in this structure: DD/MM/YYYY";
            this.info3_label.Click += new System.EventHandler(this.info3_label_Click);
            // 
            // Sign_Up
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightYellow;
            this.BackgroundImage = global::gui_client_igoad.Properties.Resources.background1;
            this.ClientSize = new System.Drawing.Size(832, 768);
            this.Controls.Add(this.info_panel);
            this.Controls.Add(this.back_button);
            this.Controls.Add(this.sign_up_panel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "Sign_Up";
            this.Text = "Signup";
            this.sign_up_panel.ResumeLayout(false);
            this.sign_up_panel.PerformLayout();
            this.info_panel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel sign_up_panel;
        private System.Windows.Forms.Button sign_up_button;
        private System.Windows.Forms.Label invalid_message_label;
        private System.Windows.Forms.TextBox adressTextBox;
        private System.Windows.Forms.TextBox phoneNumTextBox;
        private System.Windows.Forms.TextBox usernameTextBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox emailTextBox;
        private System.Windows.Forms.Label phoneNumberLabel;
        private System.Windows.Forms.Label adressLabel;
        private System.Windows.Forms.Label emailLabel;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Label userNameLabel;
        private System.Windows.Forms.Button troll_butt;
        private System.Windows.Forms.Button back_button;
        private System.Windows.Forms.TextBox birthDateTextBox;
        private System.Windows.Forms.Label birthDateLabel;
        private System.Windows.Forms.Panel info_panel;
        private System.Windows.Forms.Label info3_label;
        private System.Windows.Forms.Label info2_label;
        private System.Windows.Forms.Label info1_label;
    }
}