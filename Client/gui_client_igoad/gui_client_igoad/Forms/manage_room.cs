﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace gui_client_igoad
{
    public partial class manage_room : Form
    {
        private readonly object listOfUsersLock = new object();
        BindingList<UserStructure> listOfUsers = new BindingList<UserStructure>();
        Thread thread;

        private delegate void ChangeUsersDataView();
        private ChangeUsersDataView CRDVDelegate;

        private delegate void ClearUserDataView();
        private ClearUserDataView ClearRDVDelegate;

        public manage_room()
        {
            InitializeComponent();
            connectedUsers.DataSource = listOfUsers;
            room_name_label.Text = Helper.room_joined;
            loggedName.Text = Helper.loggedUserName;
            num_of_questionsLabel.Text = Helper.numberOfQuestions.ToString();
            max_playersLabel.Text = Helper.maxPlayers.ToString();
            time_for_questionsLabel.Text = Helper.timeForQuestion.ToString();

            this.thread = new Thread(new ThreadStart(UpdateUsers));
            thread.Start();
            CRDVDelegate = new ChangeUsersDataView(UpdateUsersView);
            ClearRDVDelegate = new ClearUserDataView(ClearUserView);
        }

        public class UserStructure
        {
            public string name { get; set; }
            public string isAdmin { get; set; }
        }

        private void StartGameButton_Click(object sender, EventArgs e)
        {
            Helper.notImplemented();
        }

        private void UpdateUsers()
        {
            Thread.Sleep(1000);
            while (true)
            {
                GetUsersFromServer();
                lock (listOfUsersLock)
                {
                    Thread.Sleep(2500);
                }
            }
        }

        private void UpdateUsersView()
        {
            listOfUsers.Add(us);
        }

        private void ClearUserView()
        {
            listOfUsers.Clear();
        }

        UserStructure us;

        private void GetUsersFromServer()
        {
            lock (listOfUsersLock)
            {
                this.Invoke(ClearRDVDelegate);
                string players = string.Empty;
                var users = Helper.GetUsersInRoom(Helper.roomID);

                for (int i = 0; i< users.Length; i++)
                {
                    if(i==0)
                    {
                        us = new UserStructure() { name = users[i], isAdmin = "Admin" };
                    }
                    else
                    {
                        us = new UserStructure() { name = users[i], isAdmin = "" };
                    }
                    this.Invoke(CRDVDelegate);
                }                
            }
        }

        private void manage_room_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void manage_room_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                List<byte> bytes = new List<byte>();
                bytes.Add(Convert.ToByte(Helper.LOGOUT_CODE));
                bytes.AddRange(Encoding.ASCII.GetBytes(Helper.ZERO_LENGTH));
                Networking.SendData(bytes);
            }
            catch (Exception ex) { }
            Environment.Exit(1);
        }

        private void closeRoomButton_Click(object sender, EventArgs e)
        {
            Helper.notImplemented();
        }
    }
}
