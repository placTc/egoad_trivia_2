﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Windows.Forms;

namespace gui_client_igoad
{
    public partial class Sign_Up : Form
    {
        public Sign_Up()
        {
            InitializeComponent();
            info_panel.Visible = false;
        }

        private void sign_up_button_Click(object sender, EventArgs e)
        {
            string username = usernameTextBox.Text;
            string password = passwordTextBox.Text;
            string email = emailTextBox.Text;
            string adress = adressTextBox.Text;
            string phoneNumber = phoneNumTextBox.Text;
            string birthDate = birthDateTextBox.Text;

            string response = String.Empty;
            bool responseSuccess = false;

            try
            {
                List<byte> serialized = SerializationFunctions.SerializeSignupRequest(username, password, email, adress, phoneNumber, birthDate);
                if (Networking.SendData(serialized))
                {
                    response = Networking.ReceiveData();
                    responseSuccess = SerializationFunctions.ParseGenericResponse(response);
                }

                if (!responseSuccess) //supposed to get if valid from server
                {
                    invalid_message_label.ForeColor = System.Drawing.Color.Red;
                    info_panel.Visible = true;
                }
                else
                {
                    invalid_message_label.ForeColor = sign_up_panel.BackColor;
                    
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: Could not reach server / Connection severed", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(1);
            }
        }

        private void usernameTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                passwordTextBox.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void passwordTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                emailTextBox.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void emailTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                adressTextBox.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void adressTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                phoneNumTextBox.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void phoneNumTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void usernameTextBox_Enter(object sender, EventArgs e)
        {
            if (usernameTextBox.Text == "username")
                usernameTextBox.Text = "";
            usernameTextBox.ForeColor = Color.Black;
        }

        private void passwordTextBox_Enter(object sender, EventArgs e)
        {
            if (passwordTextBox.Text == "password (at least 8 chars, upper and lower case, special chars , nums)")
                passwordTextBox.Text = "";
            passwordTextBox.ForeColor = Color.Black;
        }

        private void emailTextBox_Enter(object sender, EventArgs e)
        {
            if (emailTextBox.Text == "someone@example.something")
                emailTextBox.Text = "";
            emailTextBox.ForeColor = Color.Black;
        }

        private void adressTextBox_Enter(object sender, EventArgs e)
        {
            if (adressTextBox.Text == "(Street, Apt, City)")
                adressTextBox.Text = "";
            adressTextBox.ForeColor = Color.Black;
        }

        private void phoneNumTextBox_Enter(object sender, EventArgs e)
        {
            if (phoneNumTextBox.Text == "01-2345678")
                phoneNumTextBox.Text = "";
            phoneNumTextBox.ForeColor = Color.Black;
        }

        private void troll_butt_MouseHover(object sender, EventArgs e)
        {
            string[] message = { "You've gotten trolled!!!!", "We do be doing a bit of trolling", "sussy!!!!!!!!!", "peepeepoopoo check", "steave jobs", "s(he) be(lie)ve(d)", "your computer has been hacked", "option 7 filler", "sussy baka", "option 9 filler" };
            string title = "Loser";
            Random r = new Random();
            int randomNumber = r.Next(0, message.Length);
            if (randomNumber == 7)
            {
                MessageBox.Show(String.Format("you've gotten trolled {0} times, PLUS ONE", Helper.trollCounter), title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else if (randomNumber == 9)
            {
                randomNumber = r.Next(0, 2);
                if (randomNumber == 0)
                {
                    Process.Start("https://www.youtube.com/watch?v=dQw4w9WgXcQ");
                    MessageBox.Show("rickrolled", title, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    MessageBox.Show("i could have rickrolled you", title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            else
                MessageBox.Show(message[randomNumber], title, MessageBoxButtons.OK, MessageBoxIcon.Warning);

            Helper.trollCounter++;
        }

        private void back_button_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void birthDateTextBox_Enter(object sender, EventArgs e)
        {
            if (birthDateTextBox.Text == "DD/MM/YYYY")
                birthDateTextBox.Text = "";
            birthDateTextBox.ForeColor = Color.Black;
        }

        private void birthDateTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                sign_up_button.PerformClick();
            }
        }

        private void phoneNumTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                birthDateTextBox.Focus();
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void info3_label_Click(object sender, EventArgs e)
        {

        }
    }
}
